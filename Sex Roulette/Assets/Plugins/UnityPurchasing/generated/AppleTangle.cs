#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("edUb1aReUlJWVlNjMWJYY1pVUAbtpyDIvYE3XJgqHGeL8W2qK6w4m3Vjd1VQBldYQE4SIyM/NnMQNiEnZcoffivkvt/Ij6AkyKElgSRjHJI9N3MwPD03Oic6PD0gczw1cyYgNlVQBk5dV0VXR3iDOhTHJVqtpzjekzBgJKRpVH8FuIlccl2J6SBKHObmaf6nXF1TwVjickV9J4ZvXogxRVZTUNFSXFNj0VJZUdFSUlO3wvpacxASY9FScWNeVVp51RvVpF5SUlIqczIgICY+NiBzMjAwNiMnMj0wNtFSU1VaedUb1aQwN1ZSY9KhY3lV+PAiwRQABpL8fBLgq6iwI5618B9zMj03czA2ISc6NTowMic6PD1zI2BlCWMxYlhjWlVQBldVQFEGAGJAMT82cyAnMj03MiE3cyc2IT4gczIjPzZzATw8J3MQEmNNRF5jZWNnYSlj0VIlY11VUAZOXFJSrFdXUFFSJCR9MiMjPzZ9MDw+fDIjIz82MDI3ZnBGGEYKTuDHpKXPzZwD6ZILA39zMDYhJzo1OjAyJzZzIzw/OjAqIz82cxA2ISc6NTowMic6PD1zEiY/NnMaPTB9YnVjd1VQBldYQE4SI0VjR1VQBldQQF4SIyM/NnMBPDwnNNxb53OkmP9/czwj5WxSY9/kEJwBNj86Mj0wNnM8PXMnOzogczA2IW51NHPZYDmkXtGcjbjwfKoAOQg3fGPSkFVbeFVSVlZUUVFj0uVJ0uBXVUBRBgBiQGNCVVAGV1lAWRIjIyc6NTowMic2czEqczI9KnMjMiEnY0JVUAZXWUBZEiMjPzZzGj0wfWJ3sbiC5COMXBaydJmiPiu+tOZERAr0VlovRBMFQk0ngOTYcGgU8IY800d4gzoUxyVarac43n0T9aQUHiziYwu/CVdh3zvg3E6NNiCsNA0278bNKV/3FNgIh0VkYJiXXB6dRzqCfRP1pBQeLFsNY0xVUAZOcFdLY0Vczm6geBp7SZutnebqXYoNT4WYboplLJLUBor0yuphEaiLhiLNLfIB5EjuwBF3QXmUXE7lHs8NMJsY00TcINIzlUgIWnzB4asXG6Mza81GphqLJcxgRzbyJMeaflFQUlNS8NFS+48tcWaZdoaKXIU4h/F3cEKk8v9Uvy5q0NgAc4Brl+LsyRxZOKx4r5pKIaYOXYYsDMihdlDpBtweDl6iJzs8ITonKmJFY0dVUAZXUEBeEiNMwohNFAO4Vr4NKtd+uGXxBB8GvxYtTB84A8US2pcnMVhD0BLUYNnSOjU6MDInOjw9cxImJzs8ITonKmJM1tDWSMpuFGSh+sgT3X+H4sNBi1t4VVJWVlRRUkVNOycnIyBpfHwkWw1j0VJCVVAGTnNX0VJbY9FSV2NmYWJnY2BlCUReYGZjYWNqYWJnYyEyMCc6MDZzICcyJzY+Nj0nIH1jXlVaedUb1aReUlJWVlNQ0VJSUw9zPDVzJzs2cyc7Nj1zMiMjPzowMiwS+8uqgpk1z3c4QoPw6LdIeZBMY9FX6GPRUPDzUFFSUVFSUWNeVVpVY1xVUAZOQFJSrFdWY1BSUqxjTthK2o2qGD+mVPhxY1G7S22rA1qAA/nZhom3r4NaVGTjJiZy");
        private static int[] order = new int[] { 1,50,10,48,43,30,57,23,14,13,40,34,24,45,14,42,57,28,36,20,44,43,48,24,43,38,56,37,40,39,45,53,52,52,56,36,53,48,58,59,55,48,54,51,44,45,54,58,56,49,53,52,52,54,58,56,56,58,59,59,60 };
        private static int key = 83;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
