#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("vFFELYirFYdOdLNt1F8U+HIMFiCHRm/UptdxWRkDAYfWD9MAKdQGXU/3CayUJN/Sd9+AWHSEXUIE7bq6pyfKmxjflaTrGtiqCFi9+qdSVE1D8uV5yNJym9zX3+7O2gB9A42kY6zZ3Ydoofaxj/7KjZoVtvU8EmOz2j5RHSscoh5ZGtKt2y0m7nDC1SY9N5C5mC8au68R4AFjp7knQGJHDPZapocgSm2Yw4C0CyLgSniqo2ZkI1vEYkuiF7JWIAoOaLTwM8OqA67Qsqs4Ohbn+I5KdYwqhypWKqJrSMhLRUp6yEtASMhLS0rI6LaM4wVSL7+CC1gJtpdoOhPugc29PMhZ86h6yEtoekdMQ2DMAsy9R0tLS09KSXO7ahwvQRWTjUhJS0pL");
        private static int[] order = new int[] { 12,9,5,10,7,13,10,13,10,11,10,11,13,13,14 };
        private static int key = 74;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
