﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIGTM_Element_Local : UIGTM_Element
{
    UIGTM.WaitingRoom element;
    [SerializeField] TextMeshProUGUI tmpTitle, tmpText, tmpCtaPositive, tmpCtaNegative;
    [SerializeField] Button buttonPositive, buttonNegative;
    [SerializeField] GameObject objBtnClose;

    public void ShowLocalUI(UIGTM.WaitingRoom _element)
    {
        element = _element;
        tmpText.text = element.text;
        objBtnClose.SetActive(element.display_close_button); //Display/Hide Close icon button
        if (!string.IsNullOrEmpty(element.title) && tmpTitle!=null) //Display title if exist
        {
            tmpTitle.text = element.title;
        }
        if (!string.IsNullOrEmpty(element.cta_positive)) //Display CTA if exist
        {
            tmpCtaPositive.text = element.cta_positive;
            buttonPositive.onClick.AddListener(() => element.action_positive());
            buttonPositive.onClick.AddListener(() => CloseWindow());
        }
        if (!string.IsNullOrEmpty(element.cta_negative)) //Display CTA n2 if exist
        {
            buttonNegative.gameObject.SetActive(true);
            tmpCtaNegative.text = element.cta_negative;
            buttonNegative.onClick.AddListener(() => element.action_negative());
            buttonNegative.onClick.AddListener(() => CloseWindow());
        }
        if (element.display_close_button) //Setup close icon button
        {
            objBtnClose.GetComponent<Button>().onClick.AddListener(() => CloseWindow());
            if (element.action_negative != null) //Link to negative event if exist
            {
                objBtnClose.GetComponent<Button>().onClick.AddListener(() => element.action_negative());
            }
            else if (element.action_positive != null) //link to positive event if no negative event
            {
                objBtnClose.GetComponent<Button>().onClick.AddListener(() => element.action_positive());
            }
        }
        //Background manager
        if (element.background_close)
        {
            cover.GetComponent<Button>().onClick.AddListener(() => CloseWindow());
            if (element.action_negative != null) //Link to negative event if exist
            {
                cover.GetComponent<Button>().onClick.AddListener(() => element.action_negative());
            }
            else if (element.action_positive != null) //link to positive event if no negative event
            {
                cover.GetComponent<Button>().onClick.AddListener(() => element.action_positive());
            }
        }

    }
}
