﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIGTM_Localization : MonoBehaviour
{
    [System.Serializable]
    public class TextLocalization
    {
        public string name;
        public SystemLanguage language;
        public string text;
    }
    [System.Serializable]
    public class ImageLocalization
    {
        public string name;
        public SystemLanguage language;
        public Sprite sprite;
    }
    public List<TextLocalization> texts;
    public List<ImageLocalization> images;
    private void Awake()
    {
        if (GetComponent<Text>() != null)
        {
            GetComponent<Text>().text = get_text();
        }
        if (GetComponent<TextMeshProUGUI>() != null)
        {
            GetComponent<TextMeshProUGUI>().text = get_text();
        }
        if (GetComponent<Image>() != null)
        {
            GetComponent<Image>().sprite = get_image();
        }
    }
    string get_text ()
    {
        foreach(TextLocalization loc in texts)
        {
            if(loc.language == Application.systemLanguage)
            {
                return loc.text;
            }
        }
        return texts[0].text;
    }
    Sprite get_image ()
    {
        foreach (ImageLocalization img in images)
        {
            if (img.language == Application.systemLanguage)
            {
                return img.sprite;
            }
        }
        return images[0].sprite;
    }
}
