﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UIGTM : MonoBehaviour
{
    public string BASE_URL;
    public static UIGTM Instance;

    List<WaitingRoom> queue = new List<WaitingRoom>();
    public UIGTM_Element template_title_1B, template_title_2B, template_1B, template_2B;
    public List<UIGTM_Element> extra_screen;
    private bool bDowloading = false;


    #region events
    public Action<UIGTM_Class.ActionDetail> EventResultEvent;
    #endregion events



    public class WaitingRoom
    {
        public string url, name = "";
        public int display_id = 0;
        public AssetBundle asset = null;
        public string title, text, cta_positive, cta_negative = "";
        public Action action_positive, action_negative = null;
        public bool background_close = false;
        public bool display_close_button = false;
        public bool bReady = false;
        public int iExtra = 0;
    }
    void Awake()
    {
        if (Instance != null) { Destroy(gameObject); }
        else
        {
            Instance = this;
        }
    }
    private void Update()
    {
        if (isNextReady()) 
        {
            ShowNextUi(); //Showing next UI popup element
        }
    }
    void ShowNextUi ()
    {
        WaitingRoom next = queue[0];
        if (!string.IsNullOrEmpty(next.url))
        {
            DebugEditor("Show from url");
            Instantiate(next.asset.LoadAsset(next.name));
            next.asset.Unload(false);
            UIGTM_Element.Instance.popup.result_display_id = next.display_id;
            foreach(UIGTM_Class.ActionDetail _action in UIGTM_Element.Instance.popup.actions)
            {
                _action.name = UIGTM_Element.Instance.popup.name + "/" + _action.name;
            }
        }
        else if (next.iExtra > 0)
        {
            UIGTM_Element_Local popup = Instantiate(extra_screen[next.iExtra]).GetComponent<UIGTM_Element_Local>();
            popup.ShowLocalUI(next);
        }
        else
        {
            DebugEditor("Show Local");
            UIGTM_Element display = null;
            if (next.action_negative == null && string.IsNullOrEmpty(next.title)){
                display = template_1B;
            }else if(next.action_negative == null)
            {
                display = template_title_1B;
            }else if (string.IsNullOrEmpty(next.title))
            {
                display = template_2B;
            }
            else
            {
                display = template_title_2B;
            }
            UIGTM_Element_Local popup = Instantiate(display).GetComponent<UIGTM_Element_Local>();
            popup.ShowLocalUI(next);
        }
        queue.Remove(next);
    }

    public void ShowUiLocal ( string title, string text, bool background_close, bool display_close, string cta_positive, Action action_positive , string cta_negative=null, Action action_negative = null)
    {
        WaitingRoom ui = new WaitingRoom();
        ui.title = title;
        ui.text = text;
        ui.cta_positive = cta_positive;
        ui.action_positive = action_positive;
        ui.background_close = background_close;
        ui.display_close_button = display_close;
        ui.cta_negative = cta_negative;
        ui.action_negative = action_negative;
        ui.bReady = true;
        queue.Add(ui);
    }
    public void ShowUiExtra ( int id)
    {
        WaitingRoom ui = new WaitingRoom();
        ui.bReady = true;
        ui.iExtra = id;
        queue.Add(ui);
    }

    public void ShowPopupFromUrl(string url, string name, int display_id) {
        WaitingRoom ui_dynamique = new WaitingRoom();
#if UNITY_ANDROID
        url += ".android.unity3d";
#elif UNITY_IPHONE
        url += ".iphone.unity3d";
#endif
        ui_dynamique.bReady = false;
        ui_dynamique.url = url;
        ui_dynamique.name = name;
        ui_dynamique.display_id = display_id;
        queue.Add(ui_dynamique);
        StartCoroutine(_ShowPopupFromUrl(ui_dynamique));
    }
    IEnumerator _ShowPopupFromUrl(WaitingRoom ui_dynamique) { 
        using (WWW web = new WWW(ui_dynamique.url))
        {
            yield return web;
            AssetBundle remoteAssetBundle = web.assetBundle;
            if (remoteAssetBundle == null)
            {
                DebugEditor("FAIL TO LOAD");
                queue.Remove(ui_dynamique);
                yield break;
            }
            ui_dynamique.asset = remoteAssetBundle;
            ui_dynamique.bReady = true;
        }
    }

    bool isNextReady ()
    {
        if (queue.Count > 0) {
            return UIGTM_Element.Instance == null && queue[0].bReady;
        }
        return false;
    }
    public bool IsUIShown ()
    {
        return (UIGTM_Element.Instance !=null) || queue.Count>0;
    }


    public void Result (UIGTM_Class popup, UIGTM_Class.ActionDetail _event)
    {
        DebugEditor("Result " + _event.name);
        WWWForm form = new WWWForm();
        form.AddField("usersignature", AccountManager.Instance.account.signature);
        form.AddField("result", _event.name);
        form.AddField("displayid", popup.result_display_id);
        UnityWebRequest uwr = UnityWebRequest.Post(BASE_URL+"/user/event/result", form);
        uwr.SendWebRequest();

        EventResultEvent?.Invoke(_event);
    }
    void DebugEditor(string str)
    {
#if UNITY_EDITOR
        Debug.Log("Safe:" + str);
#endif
    }
}
