﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UIGTM_Class
{
    [Tooltip("Popup name (Copy/Paste)")]
    public string name;
    [Tooltip("How many seconds before automatically hiding popup (0=never)")]
    public int hide_after_seconds;
    [Tooltip("Is a click on the backgroud close the popup?")]
    public bool background_close;
    [HideInInspector] public int result_display_id;
    public enum ActionType
    {
        Close, Purchase, ContactUs, RateUs, OpenUrl, OpenUrlLocal, AddCoins, AddXp, AddLevel, AddStars, AddHints, Bonus, WatchAds, RequestNotif, Other
    }
    [Serializable]
    public class ActionDetail
    {
        [Header("Repport name")]
        public string name;
        [Header("Action")]
        public ActionType type;
        public string detail;
        [Header("Close the popup when click on button")]
        public bool close_on_click;
        [Header("Send Event on click")]
        public string trigger_name;
        public string trigger_value;
        [Header("Send Event to Platform list")]
        public bool bToServer;
        public bool bToOneSignalTrigger;
        public bool bToOneSignalTag;
        public bool bToAppsFlyer;
    }
    public List<ActionDetail> actions;

    [Serializable]
    public class AnimTransition
    {
        public float duration;
        public AnimationCurve scale_curve;
        public AnimationCurve position_horizontal_curve;
        public AnimationCurve position_vertical_curve;
        public AnimationCurve rotation_curve;
    }
    public AnimTransition animation_in;
    public AnimTransition animation_out;
}
