﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIGTM_Element : MonoBehaviour
{
    public static UIGTM_Element Instance;
    public UIGTM_Class popup;
    public RectTransform rect_popup;
    public Image cover;
    float fTimeOut;

    private void Awake()
    {
        Instance = this;
        if (GetComponent<Canvas>().renderMode == RenderMode.ScreenSpaceCamera)
        {
            GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        }
        StartCoroutine(Anim_Entrance());
        fTimeOut = Time.time + 999f;
        if(popup.hide_after_seconds>0) fTimeOut = Time.time + popup.hide_after_seconds;
        if (popup.background_close)
        {
            cover.GetComponent<Button>().onClick.AddListener(() => CloseFromBackground());
        }
    }
    private void Update()
    {
        if (Time.time > fTimeOut)
        {
            fTimeOut += 100000f;
            StartCoroutine(Anim_Out());
        }
    }
    private void OnDestroy()
    {
        Instance = null;
    }
    public void CloseFromBackground()
    {
        CloseWindow();
    }
    public void CloseButton()
    {
        CloseWindow();
    }
    public void TriggerAction( int id )
    {
        if (popup.actions != null)
        {
            if(popup.actions.Count>= id - 1)
            {
                TriggerAction(popup.actions[id]);
            }
        }
    }
    void TriggerAction (UIGTM_Class.ActionDetail action)
    {
        UIGTM.Instance.Result(popup, action);
        if (action.type == UIGTM_Class.ActionType.Close || action.close_on_click)
        {
            CloseWindow();
        }
    }
    void Destroying ()
    {
        StopAllCoroutines();
        Destroy(gameObject);
    }

    IEnumerator Anim_Entrance ()
    {
        float timeOfTravel = popup.animation_in.duration; //time after object reach a target place
        float currentTime = 0; // actual floting time
        float normalizedValue;

        Color endColor = cover.color;
        Color startColor = endColor;
        startColor.a = 0f;
        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel; // we normalize our time 
            rect_popup.anchoredPosition = new Vector2(popup.animation_in.position_horizontal_curve.Evaluate(normalizedValue), popup.animation_in.position_vertical_curve.Evaluate(normalizedValue));
            rect_popup.localScale = new Vector3(popup.animation_in.scale_curve.Evaluate(normalizedValue), popup.animation_in.scale_curve.Evaluate(normalizedValue), 1f);
            rect_popup.localEulerAngles = new Vector3(0, 0, popup.animation_in.rotation_curve.Evaluate(normalizedValue));
            cover.color = Color.Lerp(startColor, endColor, normalizedValue);
            yield return null;
        }
    }
    public void CloseWindow ()
    {
        StartCoroutine(Anim_Out());
    }
    IEnumerator Anim_Out()
    {
        float timeOfTravel = popup.animation_out.duration; //time after object reach a target place
        float currentTime = 0; // actual floting time
        float normalizedValue;

        Color endColor = cover.color;
        Color startColor = endColor;
        endColor.a = 0f;
        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel; // we normalize our time 
            rect_popup.anchoredPosition = new Vector2(popup.animation_out.position_horizontal_curve.Evaluate(normalizedValue), popup.animation_out.position_vertical_curve.Evaluate(normalizedValue));
            rect_popup.localScale = new Vector3(popup.animation_out.scale_curve.Evaluate(normalizedValue), popup.animation_out.scale_curve.Evaluate(normalizedValue), 1f);
            rect_popup.localEulerAngles = new Vector3(0, 0, popup.animation_out.rotation_curve.Evaluate(normalizedValue));
            cover.color = Color.Lerp(startColor, endColor, normalizedValue);
            yield return null;
        }
        Destroying();
    }
    void DebugEditor(string str)
    {
#if UNITY_EDITOR
        Debug.Log("Safe:" + str);
#endif
    }
}
