﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wheelVerticalContent : MonoBehaviour {
	[SerializeField] RectTransform rect,rectWheel;
	
	// Update is called once per frame
	void Update () {
		Vector2 v2 = new Vector2(0,0);
		v2.y = (8.75f*rectWheel.localEulerAngles.z)-56;
		rect.anchoredPosition = v2;
	}
}
