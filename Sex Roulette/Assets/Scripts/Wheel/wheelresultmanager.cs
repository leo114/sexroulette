﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class wheelresultmanager : MonoBehaviour {
	RectTransform rect;
	[SerializeField] TextMeshProUGUI tmp,tmp2;
	void Start () {
		rect = GetComponent<RectTransform>();
	}
	
	void Update () {
		Color colText = Color.white;
		if(rect.localScale.x>0.72f){
			colText.a = (rect.localScale.x);
		}else{
			colText.a = 0.1f;
		}
		tmp.color=colText;
		tmp2.color = colText;
	}
}
