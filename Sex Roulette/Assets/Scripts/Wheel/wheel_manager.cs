﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using sexroulette;

public class wheel_manager : MonoBehaviour {

    public Rigidbody2D rigidWheel;
    [SerializeField] RectTransform rectWheel;
    private bool launchinprogress;
    [SerializeField] TextMeshProUGUI[] tmpTitle,tmpTitle2,tmpDescr;
    [SerializeField] GameObject[] Lockers;
    [SerializeField] Animator[] animWheels;
    [SerializeField] Button btnShowPremium;
    [SerializeField] GameObject BtnGoHot;
    [SerializeField] GameObject objResultRight;
    [SerializeField] Transform result;
    [SerializeField] Image[] imgWheels;
    public bool unlocked;
    private string strMode;
    private int iRolled;

    private void Awake()
    {
        IAPManager.PremiumUnlockedEvent += UnlockedWheel;
    }
    private void OnDestroy()
    {
        IAPManager.PremiumUnlockedEvent -= UnlockedWheel;
    }
    void Start()
    {
        StartCoroutine(ieLoadImage());
        Invoke("Startt", 1f);
        if (!data.PremiumWheel)
        {
            Premium.ShowPremium("Wheel_Open", data.GameType.Wheel);
        }
        else
        {
            Waiting.ShowWaiting((cancel) =>
            {
                clickback();
            });
        }
    }

    void Startt()
    {
        EventManager.Instance.IncreaseTrigger("open_kamaroulette", true, true, true, false);
        EventManager.Instance.IncreaseTrigger("scene_game_opened", true, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("open_kamaroulette");
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("scene_game_opened");

        iRolled = 0;
        unlocked = data.PremiumWheel;
        BtnGoHot.SetActive(!unlocked);
        selectWheel("0");
        BtnGoHot.GetComponent<Animator>().SetTrigger("anim");
        if (!data.PremiumWheel)
        {
            //Premium.ShowPremium("Wheel_Open", data.GameType.Wheel);
        }
        else
        {
            Waiting.HideWaiting();
        }
    }
    public void ClickPremium ()
    {
        Premium.ShowPremium("Wheel_Game", data.GameType.Wheel);
    }
    void UnlockedWheel ( bool b) {
        unlocked=true;
        BtnGoHot.SetActive(false);
        selectWheel(strMode);
    }
    public void selectWheel ( string strMode2 ) {
        strMode = strMode2;
        string _MyLocalizedString;
        for ( int i = 0 ; i < 9 ; i++ ) {
            btnShowPremium.interactable=!unlocked;
            _MyLocalizedString = I2LocManager.get_text("Wheel/mode/" +strMode+"/result/"+i.ToString()+"/title");
            tmpTitle[i].text = _MyLocalizedString;
            tmpTitle2[i].text = _MyLocalizedString;
            Lockers[i].SetActive( (!unlocked) && (i%3!=0) );
            if( (unlocked) || (i%3==0) ) _MyLocalizedString = I2LocManager.get_text("Wheel/mode/" +strMode+"/result/"+i.ToString()+"/descr");
            else{
                _MyLocalizedString = I2LocManager.get_text("Wheel/Unlock to see more");
            }
            if(data.Soft)tmpDescr[i].text = "";
            else tmpDescr[i].text = getdescription(_MyLocalizedString);
        }
        for ( int i = 0 ; i < animWheels.Length ; i++ ){
            if(i==int.Parse(strMode)){
                if(animWheels[i].GetCurrentAnimatorStateInfo(0).IsName("wheelMiniatureSmall")){
                    animWheels[i].SetTrigger("gobig");
                }
            }else{
                if(animWheels[i].GetCurrentAnimatorStateInfo(0).IsName("wheelMiniatureBig")){
                    animWheels[i].SetTrigger("gosmall");
                }
            }
        }
    }
    string getdescription ( string str ) {
        Player pMale = getRandomPlayer(true);
        Player p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        while (p2 == pMale) p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];

        Debug.Log("Player Male " + pMale.name);
        Debug.Log("Player Other " + p2.name);
        str = str.Replace("{Player_Allsex}", p2.name);
        str = str.Replace("{Player_Man}",pMale.name);
        return str;
    }
    Player getRandomPlayer ( bool bMale )
    {
        int iTry = 0;
        Player p = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        while (p.isMale != bMale && iTry<20)
        {
            p = data.players[UnityEngine.Random.Range(0, data.players.Count)];
            iTry++;
        }
        return p;
    }

    public void LaunchWheel () {
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("Wheel_Launched");
        EventManager.Instance.IncreaseTrigger("Wheel_Launched", false, true, true, false);
        EventManager.Instance.SendEvent("Wheel_In_Progress", iRolled.ToString(), true, true, true, false);
        objResultRight.SetActive(true);
        if(launchinprogress) return;
        iRolled++;
        rigidWheel.angularDrag=0f;
        rigidWheel.AddTorque(Random.Range(100,300));
        StartCoroutine(waitandcheck());
    }
    IEnumerator waitandcheck () {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0,3.2f));
        rigidWheel.angularDrag=0.35f;
        launchinprogress=true;
    }
    void Update () {
        if(launchinprogress){
            if(rigidWheel.angularVelocity<30f) StartCoroutine(ieWheelStop());
            //Debug.Log(rigidWheel.angularVelocity);
        }
    }
    IEnumerator ieWheelStop () {
        launchinprogress=false;
        rigidWheel.angularDrag=0f;
        bool bStop = false;
        float fModulo = 40f;
        if(!unlocked) fModulo=120f;
        float fLastAngle = rectWheel.localEulerAngles.z%fModulo;
        float fFinaleAngle = rectWheel.localEulerAngles.z +(fModulo-rectWheel.localEulerAngles.z%fModulo);
        while(!bStop){
            yield return new WaitForSeconds(0.01f);
            if(rectWheel.localEulerAngles.z%fModulo<fLastAngle){
                rigidWheel.angularVelocity=0f;
                rigidWheel.angularDrag=999999999f;
                bStop=true;
            }
            fLastAngle = rectWheel.localEulerAngles.z%fModulo;
        }
        Debug.Log("STTTTOOOPPPPP ");
        Transform winner = null;
        foreach(Transform item in objResultRight.transform.Find("Content"))
        {
            if (winner == null) winner = item;
            else
            {
                if(item.GetComponent<RectTransform>().localScale.x > winner.GetComponent<RectTransform>().localScale.x)
                {
                    winner = item;
                }
            }
        }
        Debug.Log("Winner is " + winner.name);
        result.Find("Title").GetComponent<TextMeshProUGUI>().text = winner.Find("resultitle").GetComponent<TextMeshProUGUI>().text;
        result.Find("Description").GetComponent<TextMeshProUGUI>().text = winner.Find("resultdescription").GetComponent<TextMeshProUGUI>().text;
        if (data.Soft) result.Find("Description").GetComponent<TextMeshProUGUI>().text = "";
        result.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.01f);
        rectWheel.localEulerAngles = new Vector3(0,0,fFinaleAngle);
        if(iRolled>5 && !PlayerPrefs.HasKey("sexrouletterated_wheel")){
            PlayerPrefs.SetInt("sexrouletterated_wheel",1);
            Invoke("RateAsk",3f);
        }
        BtnGoHot.GetComponent<Animator>().SetTrigger("anim");
    }
    void RateAsk () {
        data.ShowRateUsPopup();
    }
    public void clickback () {
        Destroy(gameObject);
        //menumanager.QuitGameMode(gameObject);
    }
    IEnumerator ieLoadImage()
    {
        string _MyLocalizedString = I2LocManager.get_text("Wheel/logo");
        Debug.Log("Try to load " + _MyLocalizedString);
        Sprite spr = Resources.Load<Sprite>("logos/wheel_en");
        foreach (Image img in imgWheels) img.sprite = spr;

        string pathImage = Application.persistentDataPath + "/" + _MyLocalizedString;
        if (System.IO.File.Exists(pathImage))
        { //if the image is saved on the device
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            spr = Sprite.Create(load_texture, new Rect(0, 0, load_texture.width, load_texture.height), new Vector2(0, 0));
        }
        else
        { //if the image is not saved on the device, download it from the server
            string urlImage = "https://d2eeazcf1if3qf.cloudfront.net/sexroulette/" + _MyLocalizedString;
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    spr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                }
            }
        }
        if (spr != null)
        {
            foreach (Image img in imgWheels) img.sprite = spr;
        }
    }
}
