﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using I2.Loc;

public class TermPrivacy : MonoBehaviour
{
    private TextMeshProUGUI title;
    private TextMeshProUGUI tmp;
    [SerializeField] TextAsset txtPrivacy, txtTerms;
    private void Awake()
	{
        title = transform.Find("Title").GetComponent<TextMeshProUGUI>();
        transform.Find("CloseButton").GetComponent<Button>().onClick.AddListener(() => Back());
		tmp = transform.Find("Scroll View").Find("Viewport").Find("Content").GetComponent<TextMeshProUGUI>();
    }
    public void LoadContent ( string str)
    {
        if (str == "privacy")
        {
            tmp.text = txtPrivacy.text;
            title.text = I2LocManager.get_text("premium/Privacy");
        }
        else if (str == "terms")
        {
            tmp.text = txtTerms.text;
            title.text = I2LocManager.get_text("premium/Terms");
        }

    }
    void Back ()
    {
        Destroy(gameObject);
    }
}
