﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

public class Premium : MonoBehaviour
{
    private static Premium Instance;
    public GameObject prefabTermsPrivacy;
    public List<GameInfo> infos;
    public Image main_image;
    public List<Image> secondary_images;
    public TextMeshProUGUI tmpPrice;
    public TextMeshProUGUI tmpDescription;
    public TextMeshProUGUI tmpSubtitle;
    public TextMeshProUGUI tmpTitle;
    public RawImage imgRawBackground;

    string click_from = "";
    [Serializable]
    public class GameInfo
    {
        public data.GameType gameType;
        public Sprite sprite;
        public string filename,i2loc_key;
    }

    private void Awake()
    {
        Instance = this;
        IAPManager.PremiumUnlockedEvent += Unlocked;
        transform.Find("BackButton").GetComponent<Button>().onClick.AddListener(() => _HidePremium());
        _HidePremium();
    }
    private void OnDestroy()
    {
        IAPManager.PremiumUnlockedEvent -= Unlocked;
    }
    void Unlocked( bool b)
    {
        _HidePremium();
    }
    public static void ShowPremium (string _click_from, data.GameType gameType, int iKey = 0)
    {
        Instance._ShowPremium(_click_from, gameType, iKey);
    }

    void _ShowPremium ( string _click_from, data.GameType gameType, int iKey = 0)
    {
        gameObject.SetActive(true);
        click_from = _click_from;
        data.GroupSplit group = data.SplitGroup;
        tmpSubtitle.text = I2LocManager.get_text(group.subtitle_key);
        int id_secondary = 0;
        foreach(GameInfo info in infos)
        {
            if (info.gameType == gameType)
            {
                main_image.sprite = info.sprite;
                if(info.gameType == data.GameType.Scenario) tmpDescription.text = I2LocManager.get_text(info.i2loc_key+iKey.ToString());
                else tmpDescription.text = I2LocManager.get_text(info.i2loc_key);
                StartCoroutine(ieLoadBackground(info.filename));
            }
            else
            {
                if (secondary_images.Count > id_secondary)
                {
                    secondary_images[id_secondary].sprite = info.sprite;
                    id_secondary++;
                }
            }
        }
        string text = I2LocManager.get_text(group.pricing_key);
        text = text.Replace("{price}", data.PriceWeekly);
        tmpPrice.text = text;

    }
    public static void HidePremium ()
    {
        Instance._HidePremium();
    }
    void _HidePremium ()
    {
        gameObject.SetActive(false);
    }
    IEnumerator ieLoadBackground(string strFileName)
    {
        imgRawBackground.gameObject.SetActive(false);
        Debug.Log("LOAD BACKGROUND " + name);
        string pathImage = Application.persistentDataPath + "/" + strFileName;
        if (System.IO.File.Exists(pathImage))
        { //if the image is saved on the device
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            imgRawBackground.texture = load_texture;
            imgRawBackground.gameObject.SetActive(true);
        }
        else
        { //if the image is not saved on the device, download it from the server
            string urlImage = "https://101fitness.s3.eu-west-2.amazonaws.com/sexroulette/" + strFileName; //image url
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    imgRawBackground.texture = texture;
                    imgRawBackground.gameObject.SetActive(true);
                }
            }
        }
        if (imgRawBackground.gameObject.activeInHierarchy)
        { //resize picture
            //float fRatio = GetComponent<RectTransform>().localScale.y;
            //float fImageRatio = 1242f / 2688f; //GetComponent<RectTransform>().localScale.y;
            //RectTransform rectBackground = imgRawBackground.GetComponent<RectTransform>();
            //rectBackground.sizeDelta = new Vector2(Screen.width / fRatio, fImageRatio * Screen.width / fRatio);//(Screen.height * 1125) / (fRatio * 2436), Screen.height / fRatio); //(Screen.width * 2208) / (fRatio * 1242f));
            //rectBackground.anchoredPosition = new Vector2(0, 0);
            //if (rectBackground.sizeDelta.y < Screen.height / fRatio)
            //{
            //    rectBackground.sizeDelta = new Vector2((Screen.height / fRatio) / (fImageRatio), Screen.height / fRatio);//Screen.width / fRatio, (Screen.width * 2436) / (1125 * fRatio));//(Screen.height * 1242) / (2208f * fRatio), Screen.height / fRatio);
            //}
            float fWidth = GetComponent<RectTransform>().rect.width;
            float fHeight = GetComponent<RectTransform>().rect.height;

            //float fRatio = GetComponent<RectTransform>().localScale.x;
            RectTransform rectBackground = imgRawBackground.GetComponent<RectTransform>();
            rectBackground.sizeDelta = new Vector2(fWidth, (fWidth * 1242f) / (2688f));
            rectBackground.anchoredPosition = new Vector2(0, 0);
            if (rectBackground.sizeDelta.y < fHeight)
            {
                rectBackground.sizeDelta = new Vector2((fHeight * 2688f) / (1242f), fHeight);
            }
            rectBackground.localScale = new Vector3(1, 1, 1);

        }
        yield return new WaitForSeconds(0.5f);
        Resources.UnloadUnusedAssets();
    }
    public void ClickTermsPrivacy ( string str)
    {
        TermPrivacy item = Instantiate(prefabTermsPrivacy).GetComponent<TermPrivacy>();
        item.LoadContent(str);
    }
    public void Purchase ()
    {
        IAPManager.PurchasePack("", click_from);
    }
}
