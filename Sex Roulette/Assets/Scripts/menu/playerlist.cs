﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using sexroulette;

public class playerlist : MonoBehaviour {
	[SerializeField] menumanager menuManager;
    [SerializeField] Transform parent;
    private Transform child;
	//LANGUAGE
	[SerializeField] Image imgFlag;
	[SerializeField] GameObject objFlagList;
    [SerializeField] GameObject objRestore;
    [SerializeField] GameObject objGenderWarning;

    void Awake () {
#if UNITY_ANDROID
			Destroy(objRestore);
#endif
        child = parent.Find("Entry");
        child.gameObject.SetActive(false);
        parent.Find("AddPlayerButton").GetComponent<Button>().onClick.AddListener(() => AddPlayer());
    }
	void Start () {
        data.LoadPlayers();
        foreach (Player p in data.players)
        {
            InstantPlayer(p,false);
        }
        string _MyLocalizedString = I2LocManager.get_text("flagImageName");
		imgFlag.sprite = Resources.Load<Sprite>("flags/"+_MyLocalizedString);
	}
    void AddPlayer ()
    {
        Player p = new Player();
        p.name = "";
        p.isMale = false;
        p.bSexSelected = false;
        p.rdmId = UnityEngine.Random.Range(0, 99999).ToString();
        data.players.Add(p);
        InstantPlayer(p, true);
    }
    void InstantPlayer ( Player p , bool bNew )
    {
        RectTransform rect = Instantiate(child).GetComponent<RectTransform>();
        rect.SetParent(parent);
        rect.name = p.rdmId;
        rect.localScale = new Vector3(1, 1, 1);
        rect.gameObject.SetActive(true);
        rect.Find("MaleButton").GetComponent<Button>().onClick.AddListener(() => SelectSex(rect,p,true));
        rect.Find("FemaleButton").GetComponent<Button>().onClick.AddListener(() => SelectSex(rect, p, false));
        rect.Find("RemoveButton").GetComponent<Button>().onClick.AddListener(() => RemovePlayer(rect, p));
        rect.Find("Field").GetComponent<TMP_InputField>().onEndEdit.AddListener( delegate { PlayerNameChanged(rect, p); });
        if (bNew)
        {
            rect.Find("Field").GetComponent<TMP_InputField>().Select();
        }
        else
        {
            DisplayUserInfo(rect, p);
        }
    }
    void SelectSex ( RectTransform rect, Player p , bool bMale)
    {
        StopAllCoroutines();
        p.isMale = bMale;
        p.bSexSelected = true;
        DisplayUserInfo(rect, p);
        data.SavePlayers();
        objGenderWarning.SetActive(false);
    }
    void PlayerNameChanged ( RectTransform rect , Player p)
    {
        TMP_InputField field = rect.Find("Field").GetComponent<TMP_InputField>();
        if (field.text == "debug")
        {
            SendDebug();
            RemovePlayer(rect, p);
            return;
        }
        else if (field.text == "reset")
        {
            data.Premium = false;
            PlayerPrefs.DeleteKey("wheelunlocked");
            PlayerPrefs.DeleteKey("neverunlocked");
            PlayerPrefs.DeleteKey("sexroulette_premium_dice");
            PlayerPrefs.DeleteKey("sexrouletteunlock0");
            PlayerPrefs.DeleteKey("sexrouletteunlock1");
            PlayerPrefs.DeleteKey("sexrouletteunlock2");
            PlayerPrefs.DeleteKey("tod4buypack1");
            PlayerPrefs.DeleteKey("tod4buypack2");
            PlayerPrefs.DeleteKey("tod4buypack3");
            PlayerPrefs.DeleteKey("tod4buypack4");
            RemovePlayer(rect, p);
            return;
        }else if (field.text.Contains("split/"))
        {
            string str = field.text.Replace("split/", "");
            int iVal = int.Parse(str);
            PlayerPrefs.SetString("Split_Info",str);
            EventManager.Instance.SendEvent("Split", str, true, true, false, false);
            RemovePlayer(rect, p);
            return;
        }
        p.name = rect.Find("Field").GetComponent<TMP_InputField>().text;
        DisplayUserInfo(rect,p);
        data.SavePlayers();
    }
    void RemovePlayer ( RectTransform rect , Player p)
    {
        data.players.Remove(p);
        Destroy(rect.gameObject);
        data.SavePlayers();
    }
    void DisplayUserInfo ( RectTransform rect , Player p)
    {
        if (p.name.Length == 0)
        {
            RemovePlayer(rect, p);
            return;
        }
        rect.Find("Field").GetComponent<TMP_InputField>().text = p.name;
        Color colMale = rect.Find("MaleButton").GetComponent<Image>().color;
        Color colFemale = rect.Find("FemaleButton").GetComponent<Image>().color;
        if (p.isMale)
        {
            colMale.a = 1f;
            colFemale.a = 0f;
        }
        else
        {
            colMale.a = 0f;
            colFemale.a = 1f;
        }
        if (!p.bSexSelected)
        {
            colMale.a = 0f;
            colFemale.a = 0f;
        }
        rect.Find("MaleButton").GetComponent<Image>().color = colMale;
        rect.Find("FemaleButton").GetComponent<Image>().color = colFemale;
    }

    public void ClickNext () {
        if (data.players.Count < 2)
        {
            AddPlayer();
            return;
        }
        bool bOk = true;
        foreach(Player p in data.players)
        {
            if (!p.bSexSelected)
            {
                bOk = false;
                StartCoroutine(AnimSelectGender(p));
            }
        }
        if (bOk)
        {
            EventManager.Instance.SendEvent("PlayerNumber", data.players.Count.ToString(), true,false, true, false);
            //OneSignalInit.SendTag("PlayerNumber", data.players.Count.ToString());
            menuManager.PlayerNameEnded();
        }
	}
	IEnumerator AnimSelectGender ( Player p )
    {
        objGenderWarning.SetActive(true);
        Transform rect = parent.Find(p.rdmId);
        Color colMale = rect.Find("MaleButton").GetComponent<Image>().color;
        Color colFemale = rect.Find("FemaleButton").GetComponent<Image>().color;
        for (int i = 0; i < 5; i++)
        {
            colMale.a = 1f;
            colFemale.a = 1f;
            rect.Find("MaleButton").GetComponent<Image>().color = colMale;
            rect.Find("FemaleButton").GetComponent<Image>().color = colFemale;
            yield return new WaitForSeconds(0.2f);
            colMale.a = 0f;
            colFemale.a = 0f;
            rect.Find("MaleButton").GetComponent<Image>().color = colMale;
            rect.Find("FemaleButton").GetComponent<Image>().color = colFemale;
            yield return new WaitForSeconds(0.2f);
        }
    }

    public void ClickFlag () {
		objFlagList.SetActive(!objFlagList.activeInHierarchy);
	}
	public void ChangeLanguage ( string str ) {
        string title = I2LocManager.get_text("Change Language Alert");
        string message = I2LocManager.get_text("Change Language Text");
        StanManager.ShowMessage1CTA(title, message, "OK", () => {
            I2.Loc.LocalizationManager.CurrentLanguage = str;
            Application.Quit();
        });
        }

    public void RateUs () {
        Special.RateUs();
	}
	public void Restore () {
        IAPManager.RestorePurchase();
	}
    public void clickContact()
    {
        string _title = I2LocManager.get_text("Contact Us");
        string _text = I2LocManager.get_text("ContactPopup");
        string _yes = I2LocManager.get_text("ContactCTAYes");
        string _no = I2LocManager.get_text("ContactCTANo");
        StanManager.ShowMessage2CTA(_title, _text, _yes, _no, () => {
            Special.ContactUs();
        }, 
        () => {
            Debug.Log("No button pressed");
        });
    }
    void SendDebug() {
        string strDebug = "";
        foreach (string str in data.Debugs)
        {
            strDebug += "\n" + str;
        }
        string email = "app@greentomatomedia.com";
        //subject of the mail
        string subject = MyEscapeURL("[SexRoulette] Support");
        //body of the mail which consists of Device Model and its Operating System
        string body = MyEscapeURL(strDebug);
        //Open the Default Mail App
        Application.OpenURL ("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL (string url){
        return WWW.EscapeURL(url).Replace("+","%20");
    }

}
