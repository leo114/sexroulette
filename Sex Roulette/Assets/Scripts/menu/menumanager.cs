﻿using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class menumanager : MonoBehaviour {
	
	private static menumanager instance;
	[SerializeField] GameObject CanvasPlayer,CanvasMode;
	[SerializeField] GameObject prefabSexGame,prefabTOD,prefabSexyDice,prefabWheel,prefabNever;
    [SerializeField] GameObject MaxPlayerObj;

    [SerializeField] List<GameObject> to_activate;

    void Awake () {
		instance = this;
	}
	void Start () {
#if UNITY_EDITOR
        //PlayerPrefs.DeleteAll();
#endif
        EventManager.Instance.SendEvent("Version", data.AppVersion, false, false, true, false);
        foreach (GameObject obj in to_activate) obj.SetActive(true);
	}
    public void PlayerNameEnded () {
		CanvasPlayer.SetActive(false);
		CanvasMode.SetActive(true);
        MaxPlayerObj.SetActive(data.players.Count > 3);
    }
	public static void _clickGame ( string str ) {
		instance.clickGame(str);
	}
	public void clickGame ( string str ) {
		switch (str){
		case "SexGame":
                Instantiate(prefabSexGame);
                //StartCoroutine(ShowMode(prefabSexGame));
			break;
        case "TOD":
                Instantiate(prefabTOD);
                //StartCoroutine(ShowMode(prefabTOD));
            break;
        case "Never":
                Instantiate(prefabNever);
                //StartCoroutine(ShowMode(prefabNever));
            break;
        case "SexyDice" :
            if (data.players.Count > 2)
            {
                ShowPlayerAmountWarning(ok => {
                    Instantiate(prefabSexyDice);
                    //StartCoroutine(ShowMode(prefabSexyDice));
                });
            }
            else
                {
                    Instantiate(prefabSexyDice);
                    //StartCoroutine(ShowMode(prefabSexyDice));
            }
		    break;
		case "Wheel" :
                if (data.players.Count > 2)
                {
                    ShowPlayerAmountWarning(ok=> {
                        Instantiate(prefabWheel);
                        //StartCoroutine(ShowMode(prefabWheel));
                    });
                }
                else
                {
                    Instantiate(prefabWheel);
                    //StartCoroutine(ShowMode(prefabWheel));
                }
			break;
		}
	}
	//IEnumerator ShowMode ( GameObject obj ) {
	//	Animator anim = Instantiate(Transi).GetComponent<Animator>();
 //       float fTimeOut = Time.time + 0.5f;
 //       while(anim.GetCurrentAnimatorStateInfo(0).IsName("transition") && Time.time<fTimeOut)
 //       {
 //       	yield return null;
 //       }
 //       Instantiate(obj);
	//	Destroy(anim.gameObject,0.5f);
	//}
	//public static void QuitGameMode ( GameObject obj ) {
	//	instance._QuitGameMode(obj);
	//}
	//void _QuitGameMode ( GameObject obj ){
	//	StartCoroutine(_ieQuitGameMode(obj));
	//} IEnumerator _ieQuitGameMode ( GameObject obj ){
	//	Animator anim = Instantiate(Transi).GetComponent<Animator>();
 //       float fTimeOut = Time.time + 0.5f;
 //       while (anim.GetCurrentAnimatorStateInfo(0).IsName("transition") && Time.time < fTimeOut)
 //       {
 //           yield return null;
 //       }
 //       Destroy(obj);
	//	Destroy(anim.gameObject,0.5f);
	//}
	public void back () {
		SceneManager.LoadScene("main");
	}
    void ShowPlayerAmountWarning(Action<bool> action)
    {
        DateTime dt = DateTime.Now;
        if (PlayerPrefs.HasKey("ShowWarning_" + dt.ToString("yyyyMMdd")))
        {
            action(true);
            return;
        }
        else
        {
            PlayerPrefs.SetInt("ShowWarning_" + dt.ToString("yyyyMMdd"), 1);
            string _title = I2LocManager.get_text("Game Mode/tipsplayers>2_title");
            string _text = I2LocManager.get_text("Game Mode/tipsplayers>2_text");
            string _ok = I2LocManager.get_text("Game Mode/tipsplayers>2_go");
            StanManager.ShowMessage1CTA(_title, _text, _ok, () => { action(true); });
            //    var builder = new UM_NativeDialogBuilder(_title, _text);
            //    builder.SetPositiveButton(_ok, () =>
            //    {
            //        action(true);
            //    });

            //    var dialog = builder.Build();
            //    dialog.Show();
            //}
        }
    }
}
