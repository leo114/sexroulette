﻿//#define NOONESIGNAL

using System;
using System.Collections.Generic;
using UnityEngine;
#if !NOONESIGNAL
using OneSignalPush.MiniJSON;
#endif

public class OneSignalManager : MonoBehaviour
{
    public string APP_ID = "";
    public static OneSignalManager Instance;
    [System.Serializable]
    public class ResultAction
    {
        public string id, action;
    }
    private void Awake()
    {
        if (Instance) Destroy(gameObject);
        else Instance = this;
        Initialize();
    }
    void Initialize()
    {
#if !NOONESIGNAL
		OneSignal.StartInit(APP_ID)
			.Settings(new Dictionary<string, bool>() {
    { OneSignal.kOSSettingsAutoPrompt, false },
    { OneSignal.kOSSettingsInAppLaunchURL, true } })
			.HandleNotificationOpened(HandleNotificationOpened)
			.HandleInAppMessageClicked(HandleInAppMessageClicked)
			.EndInit();

		OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;
		OneSignal.SetLocationShared(false);
#endif
        InvokeRepeating("GetIdOneSignal", 5f, 30f);
	}
    void GetIdOneSignal()
    {
#if !NOONESIGNAL
        var status = OneSignal.GetPermissionSubscriptionState();
        if(!string.IsNullOrEmpty(status.subscriptionStatus.userId))
            AccountManager.Instance.SetOneSignal(status.subscriptionStatus.userId);
        if (!PlayerPrefs.HasKey("SplitNumberSent"))
        {
            PlayerPrefs.SetInt("SplitNumberSent",1);
            EventManager.Instance.SendEvent("Split", data.Split, true, true, false, false);
        }
#endif
    }


    public void TriggerAskNotif()
    {
#if !NOONESIGNAL
		OneSignal.PromptForPushNotificationsWithUserResponse(OneSignal_promptForPushNotificationsReponse);
#endif
    }

    private void OneSignal_promptForPushNotificationsReponse(bool accepted) {
#if !NOONESIGNAL
		Debug.Log("OneSignal_promptForPushNotificationsReponse: " + accepted);
#endif
	}
        
#if !NOONESIGNAL
    private static void HandleNotificationOpened(OSNotificationOpenedResult result)
    {
        OSNotificationPayload payload = result.notification.payload;
        Dictionary<string, object> additionalData = payload.additionalData;
        string message = payload.body;
        string actionID = result.action.actionID;

        print("GameControllerExample:HandleNotificationOpened: " + message);
        string extraMessage = "Notification opened with text: " + message;

        if (additionalData != null)
        {
            if (additionalData.ContainsKey("action"))
            {
                extraMessage += "add:" + (string)additionalData["action"];
                string strAction = (string)additionalData["action"];
                ResultAction resultAction = JsonUtility.FromJson<ResultAction>(strAction);
                data.NotificationId = resultAction.id;
                if (resultAction.action.Contains("purchase"))
                {
                    string[] split = resultAction.action.Split("/"[0]);
                    IAPManager.PurchasePack(split[1], resultAction.id);
                }
                else if (resultAction.action.Contains("tag"))
                {
                    string[] split = resultAction.action.Split("/"[0]);
                    Instance.SendTag(split[1], "1");
                }
                else
                {
                    switch (resultAction.action)
                    {
                        case "rateapp":
                            Special.RateUs();
                            break;
                        case "contactus":
                            Special.ContactUs();
                            break;
                    }
                }
            }
        }
    }

    public static void HandleInAppMessageClicked(OSInAppMessageAction action)
    {
        Debug.Log("Inapp clicked");

        if (action != null)
        {
            ResultAction result = JsonUtility.FromJson<ResultAction>(action.clickName);
            string[] split = result.action.Split("/"[0]);
            switch (split[0])
            {
                case "purchase":
                    IAPManager.PurchasePack(split[1], result.id);
                    break;
                case "rateapp":
                    Special.RateUs();
                    break;
                case "contactus":
                    Special.ContactUs();
                    break;
            }
        }
    }
#endif

    public void SendTag(string tag, string value)
    {
#if !NOONESIGNAL
        OneSignal.SendTag(tag, value);
#endif
    }
    public void RemoveTag(string tag)
    {
#if !NOONESIGNAL
        OneSignal.DeleteTag(tag);
#endif
    }
    public void SendTrigger(string tag, string value)
    {
#if !NOONESIGNAL
        OneSignal.AddTrigger(tag, value);
#endif
    }
//    public static void IncrementTagTrigger(string name, bool bTag, bool bTrigger, bool bAppsFlyer)
//    {
//        if (PlayerPrefs.HasKey(name))
//        {
//            PlayerPrefs.SetInt(name, PlayerPrefs.GetInt(name) + 1);
//        }
//        else
//        {
//            PlayerPrefs.SetInt(name, 1);
//        }
//#if !NOONESIGNAL
//        if (bTrigger) OneSignal.AddTrigger(name, PlayerPrefs.GetInt(name).ToString());
//        if(bTag) OneSignal.SendTag(name, PlayerPrefs.GetInt(name).ToString());
//#endif
    //    if (bAppsFlyer)
    //    {
    //        AppsFlyerManager.Instance.SendEvent(name, PlayerPrefs.GetInt(name).ToString());
    //    }
    //}
    //public static int getTriggerAmount ( string tag)
    //{
    //    int val = PlayerPrefs.GetInt(tag, 0);
    //    Debug.Log(tag + " tag is " + val.ToString());
    //    return val;
    //}

    public Action<string> returnTagAction = null;
    public void GetTagsOneSignal(Action<string> returnTagAction2)
    {
#if !NOONESIGNAL
        Debug.Log("Get OneSignal Tags");
        returnTagAction = returnTagAction2;
        OneSignal.GetTags(TagsReceived);
#endif
    }
    private void TagsReceived(Dictionary<string, object> tags)
    {
#if !NOONESIGNAL
        Debug.Log("Received OneSignal Tags");
        string strOut = "";
        foreach (var tag in tags)
        {
            strOut += tag.Key + ": " + tag.Value.ToString() + ", ";
        }
        strOut = strOut.Substring(0, strOut.Length - 2);
        strOut = "{" + strOut + "}";
        returnTagAction(strOut);
        Debug.Log("Get OneSignal Tags OVER");
#endif
    }
}
