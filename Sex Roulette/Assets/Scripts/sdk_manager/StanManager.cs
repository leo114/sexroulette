﻿#define NO_STANASSET
using System;
using UnityEngine;
#if !NO_STANASSET
//using SA.CrossPlatform.Media;
using SA.CrossPlatform.UI;
//using SA.CrossPlatform.Notifications;
//using SA.CrossPlatform.Social;
//using SA.Foundation.Utility;
//using SA.Android.Social;
//using SA.CrossPlatform.App;
#endif

public class StanManager : MonoBehaviour
{
    public static void ShowMessage1CTA(string title, string msg, string cta, Action action = null)
    {
#if !NO_STANASSET
        var builder = new UM_NativeDialogBuilder(title,msg);
        builder.SetPositiveButton(cta, () => {
            if (action!=null) action();
        });
        var dialog = builder.Build();
        dialog.Show();
#else
        UIGTM.Instance.ShowUiLocal(title, msg, false, false, cta, action);
#endif
    }
    public static void ShowMessage2CTA ( string title, string msg, string cta1, string cta2, Action action1, Action action2)
    {
#if !NO_STANASSET
        var builder = new UM_NativeDialogBuilder(title, msg);
        builder.SetPositiveButton(cta1, () => {
            action1();
        });
        builder.SetNegativeButton(cta2, () => {
            action2();
        });
        var dialog = builder.Build();
        dialog.Show();
#else
        UIGTM.Instance.ShowUiLocal(title, msg, false, false, cta1, action1, cta2, action2);
#endif
    }

}
