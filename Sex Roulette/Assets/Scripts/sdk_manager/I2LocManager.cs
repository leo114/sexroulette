﻿//#define NO_I2LOC
using UnityEngine;
using I2.Loc;

public class I2LocManager : MonoBehaviour
{
    public static string get_text ( string key)
    {
#if !NO_I2LOC
        LocalizedString _MyLocalizedString = key;
        return _MyLocalizedString;
#endif
        return "";
    }
}
