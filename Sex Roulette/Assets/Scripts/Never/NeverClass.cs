﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NeverClass
{
    [Serializable]
    public class NeverChallenge
    {
        public int id;
        public int round;
        public int minplayer;
        public string gender;
        public string age;
        public int mode;
        public int bgcolor;
        public bool bHard;
        public string question;
    }
}
