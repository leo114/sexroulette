﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using NeverClass;

public class NeverGame : MonoBehaviour
{

    private bool bHard = false;
    [SerializeField] GameObject objPremiumIcon;
    [SerializeField] GameObject objToggle;
    [SerializeField] TextMeshProUGUI tmpContent;
    private List<int> listDone = new List<int>();
    private int iDone = -1;
    [SerializeField] Image[] logos;

    private void Awake()
    {
        IAPManager.PremiumUnlockedEvent += UnlockedPremium;
    }
    private void OnDestroy()
    {
        IAPManager.PremiumUnlockedEvent -= UnlockedPremium;
    }
    private void Start()
    {
        EventManager.Instance.IncreaseTrigger("open_neverever", true, true, true, false);
        EventManager.Instance.IncreaseTrigger("scene_game_opened", true, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("open_neverever");
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("scene_game_opened");
        listDone = new List<int>();
        if (PlayerPrefs.HasKey("SexRouletteNeverDone"))
        {
            string strSession = PlayerPrefs.GetString("SexRouletteNeverDone");
            strSession = strSession.Replace("{" + '"' + "array" + '"' + ":", "");
            if (strSession[strSession.Length - 1] == '}') strSession = strSession.Substring(0, strSession.Length - 1);
            listDone = JsonHelper.getJsonArray<int>(strSession);
        }
        bHard = data.PremiumNever;
        DisplayCat();
        Next();
        StartCoroutine(ieLoadImage());
        if (!data.PremiumNever) ShowPremium();
    }
    public void Back ()
    {
        Destroy(gameObject);
    }
    public void Next ()
    {
        iDone++;
        tmpContent.gameObject.SetActive(false);
        NeverChallenge next_chal = NextChallenge();
        tmpContent.text = next_chal.question;
        tmpContent.gameObject.SetActive(true);
        EventManager.Instance.SendEvent("Never_In_Progress", iDone.ToString(), true, true, false, false);
        EventManager.Instance.IncreaseTrigger("Never_Done", false, true, true, false);
    }

    NeverChallenge NextChallenge ()
    {
        int iCount = 0;
        int id = UnityEngine.Random.Range(0, Never_data.challenge.Count);
        while (iCount < 100 && !isValid(Never_data.challenge[id]))
        {
            id = UnityEngine.Random.Range(0, Never_data.challenge.Count);
        }
        if (!isValid(Never_data.challenge[id]))
        {
            listDone = new List<int>();
            id = UnityEngine.Random.Range(0, Never_data.challenge.Count);
            while (iCount < 20 && !isValid(Never_data.challenge[id]))
            {
                id = UnityEngine.Random.Range(0, Never_data.challenge.Count);
            }
        }
        listDone.Add(Never_data.challenge[id].id);
        PlayerPrefs.SetString("SexRouletteNeverDone",JsonHelper.ToJson(listDone));
        return Never_data.challenge[id];
    }
    bool isValid ( NeverChallenge chal )
    {
        if (!listDone.Contains(chal.id))
        {
            return chal.bHard == bHard;
        }
        else
        {
            return false;
        }
    }
    public void SwitchMode ()
    {
        if (!data.PremiumNever)
        {
            ShowPremium();
            return;
        }
        bHard = !bHard;
        DisplayCat();
    }
    public void ShowPremium ()
    {
        Premium.ShowPremium("Never_Game", data.GameType.NeverEver);
    }
    void UnlockedPremium( bool b)
    {
        bHard = true;
        DisplayCat();
    }
    void DisplayCat ()
    {
        objPremiumIcon.SetActive(!data.PremiumNever);
        objToggle.SetActive(data.PremiumNever); 
        objToggle.transform.Find("Hard").gameObject.SetActive(bHard);
        objToggle.transform.Find("Soft").gameObject.SetActive(!bHard);
        if (!bHard) objToggle.transform.Find("Text").GetComponent<I2.Loc.Localize>().SetTerm("NeverEver/Classic");
        else objToggle.transform.Find("Text").GetComponent<I2.Loc.Localize>().SetTerm("NeverEver/Hard");
    }


    IEnumerator ieLoadImage()
    {
        string _MyLocalizedString = I2LocManager.get_text("NeverEver/logo");
        Sprite spr = Resources.Load<Sprite>("logos/never_en");// + _MyLocalizedString.ToString().Replace(".png",""));
        foreach (Image img in logos) img.sprite = spr;

        string pathImage = Application.persistentDataPath + "/" + _MyLocalizedString;
        if (System.IO.File.Exists(pathImage))
        { //if the image is saved on the device
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            spr = Sprite.Create(load_texture, new Rect(0, 0, load_texture.width, load_texture.height), new Vector2(0, 0));
        }
        else
        { //if the image is not saved on the device, download it from the server
            string urlImage = "https://d2eeazcf1if3qf.cloudfront.net/sexroulette/" + _MyLocalizedString;
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    spr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                }
            }
        }
        if (spr != null)
        {
            foreach (Image img in logos) img.sprite = spr;
        }
    }
}
