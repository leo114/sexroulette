﻿using System.Collections.Generic;
using NeverClass;
using UnityEngine;

public class Never_data : MonoBehaviour
{
    private static List<NeverChallenge> _challenge;

    public static List<NeverChallenge> challenge
    {
        get
        {
            return _challenge;
        }
        set
        {
            _challenge = value;
        }
    }
}
