﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using sexroulette;
using UnityEngine.Networking;

public class Dice_Game : MonoBehaviour
{

    [Serializable]
    public class Possiblity
    {
        public int act;
        public int bod;
        public int adv;
    }

    private bool bHard = false;
    [SerializeField] GameObject objPremiumIcon;
    [SerializeField] GameObject objToggle;
    [SerializeField] GameObject objButtonLaunch;
    [SerializeField] GameObject SpecialBefore;
    [SerializeField] GameObject SpecialAfter;

    [SerializeField] Transform tAction;
    [SerializeField] Transform tBody;
    [SerializeField] Transform tAdverb;
    [SerializeField] TextMeshProUGUI tmpPlayer;
    [SerializeField] dice_timer timer;
    private int iCount = 0;

    [SerializeField] Transform parentAction, parentBody, parentAdverb;
    [SerializeField] Transform child;
    [SerializeField] Image[] imgDices;

    private void Awake()
    {
        IAPManager.PremiumUnlockedEvent += UnlockedPremium;
    }
    private void OnDestroy()
    {
        IAPManager.PremiumUnlockedEvent -= UnlockedPremium;
    }
    private void Start()
    {
        EventManager.Instance.IncreaseTrigger("open_dice", true, true, true, false);
        EventManager.Instance.IncreaseTrigger("scene_game_opened", true, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("open_dice");
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("scene_game_opened");
        bHard = data.PremiumDice;
        DisplayCat();
        StartCoroutine(ieLoadImage());
        if (!data.PremiumDice) ShowPremium();
    }
    void ResetAll ()
    {
        foreach (Transform item in parentAction) Destroy(item.gameObject);
        foreach (Transform item in parentBody) Destroy(item.gameObject);
        foreach (Transform item in parentAdverb) Destroy(item.gameObject);
    }
    public void LaunchDice ()
    {
        iCount++;

        ResetAll();
        objButtonLaunch.SetActive(false);
        Player p1 = data.players[UnityEngine.Random.Range(0,data.players.Count)];
        Player p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        while(p1==p2) p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        string _playerLoc = I2LocManager.get_text("Sexy Dice/player key");
        tmpPlayer.text = _playerLoc.ToString().Replace("{p1}", p1.name).Replace("{p2}", p2.name);

        string _MyLocalizedString = I2LocManager.get_text("Sexy Dice/DataCode/Soft");
        if (!data.Soft)
        {
            string strP1 = "female";
            string strP2 = "female";
            if (p1.isMale) strP1 = "male";
            if (p2.isMale) strP2 = "male";
            string strMode = "Classic";
            if (bHard) strMode = "Hard";
            _MyLocalizedString = I2LocManager.get_text("Sexy Dice/DataCode/" + strMode+"/"+strP1+"/"+strP2);
        }
        string strSession = _MyLocalizedString;
        Debug.Log(strSession);
        Debug.Log(_MyLocalizedString.ToString());
        strSession = strSession.Replace("{" + '"' + "array" + '"' + ":", "");
        if (strSession[strSession.Length - 1] == '}') strSession = strSession.Substring(0, strSession.Length - 1);
        List<Possiblity> listResult = JsonHelper.getJsonArray<Possiblity>(strSession);
        int iResult = UnityEngine.Random.Range(0, listResult.Count);
        float fTime = 4.5f;
        StartCoroutine(AnimResult(parentAction, "action", fTime, listResult, iResult));
        StartCoroutine(AnimResult(parentBody, "body", fTime, listResult, iResult));
        StartCoroutine(AnimResult(parentAdverb, "adverb", fTime, listResult, iResult));
        timer.StartLaunch(fTime+1f);
    }
    IEnumerator AnimResult ( Transform parent, string str, float fTime, List<Possiblity> listResult, int iResult)
    {
        float fStart = Time.time; 
        float fEnd = fStart + fTime;
        List<int> list_id = new List<int>();
        foreach(Possiblity p in listResult)
        {
            if (str == "action") list_id.Add(p.act);
            else if (str == "body") list_id.Add(p.bod);
            else list_id.Add(p.adv);
        }
        float fSpeed = 1f;
        while (Time.time < fEnd)
        {
            float f = Time.time - fStart;
            float fWait = 0.15f+(f*0.2f);
            fSpeed = 0.6f / fWait;
            RectTransform rect = Instantiate(child).GetComponent<RectTransform>();
            rect.SetParent(parent);
            rect.gameObject.SetActive(true);
            rect.Find("Text").GetComponent<TextMeshProUGUI>().text = getText(str, list_id[UnityEngine.Random.Range(0, list_id.Count)]);
            rect.GetComponent<Animator>().SetTrigger("out");
            rect.GetComponent<Animator>().speed = fSpeed; //1.5f;
            yield return new WaitForSeconds(fWait);
        }
        RectTransform rect2 = Instantiate(child).GetComponent<RectTransform>();
        rect2.SetParent(parent);
        rect2.gameObject.SetActive(true);
        rect2.GetComponent<Animator>().speed = fSpeed;
        rect2.Find("Text").GetComponent<TextMeshProUGUI>().text = getText(str, list_id[iResult]);
    }
    string getText ( string str, int id)
    {
        string _MyLocalizedString = I2LocManager.get_text("Sexy Dice/" +str+"/"+id.ToString());
        return _MyLocalizedString;
    }
    public void TimerOver ()
    {
        objButtonLaunch.SetActive(true);
        EventManager.Instance.IncreaseTrigger("Dice_Launched", false, true, true, false);
        EventManager.Instance.SendEvent("Dice_In_Progress", iCount.ToString(), true, true, true, false);
    }

    void DisplayCat ()
    {
        objPremiumIcon.SetActive(!data.PremiumDice);
        objToggle.SetActive(data.PremiumDice);
        objToggle.transform.Find("Hard").gameObject.SetActive(bHard);
        objToggle.transform.Find("Soft").gameObject.SetActive(!bHard);
        if (!bHard) objToggle.transform.Find("Text").GetComponent<I2.Loc.Localize>().SetTerm("Sexy Dice/Classic");
        else objToggle.transform.Find("Text").GetComponent<I2.Loc.Localize>().SetTerm("Sexy Dice/Hard");
    }

    public void SwitchMode()
    {
        if (!data.PremiumDice)
        {
            ShowPremium();
            return;
        }
        bHard = !bHard;
        DisplayCat();
    }
    public void ShowPremium ()
    {
        //objPremium.SetActive(true);
        Premium.ShowPremium("dice_game", data.GameType.Dice);
    }
    void UnlockedPremium( bool b)
    {
        bHard = true;
        DisplayCat();
    }
    public void Back()
    {
        Destroy(gameObject);
    }
    IEnumerator ieLoadImage()
    {
        string _MyLocalizedString = I2LocManager.get_text("Sexy Dice/logo");
        Sprite spr = Resources.Load<Sprite>("logos/dice_en");// + _MyLocalizedString.ToString().Replace(".png", ""));
        foreach (Image img in imgDices) img.sprite = spr;

        string pathImage = Application.persistentDataPath + "/" + _MyLocalizedString;
        if (System.IO.File.Exists(pathImage))
        { //if the image is saved on the device
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            spr = Sprite.Create(load_texture, new Rect(0, 0, load_texture.width, load_texture.height), new Vector2(0, 0));
        }
        else
        { //if the image is not saved on the device, download it from the server
            string urlImage = "https://d2eeazcf1if3qf.cloudfront.net/sexroulette/" + _MyLocalizedString;
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    spr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                }
            }
        }
        if (spr != null)
        {
            foreach (Image img in imgDices) img.sprite = spr;
        }
    }

}
