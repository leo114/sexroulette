﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class dice_timer : MonoBehaviour {
    [SerializeField] Dice_Game manager;
	[SerializeField] TextMeshProUGUI tmp;
	[SerializeField] Image imgFill;
	[SerializeField] RectTransform rectRotate;
	private float fEnd,fDuration,fStartPause,fTot;
	private bool bTimerOn,bStart;
	[SerializeField] TMP_FontAsset fontBold,fontHighlight;
    [SerializeField] GameObject objSkip;
    [SerializeField] int[] timer_list;


    public void StartLaunch ( float fTime)
    {
        StopAllCoroutines();
        StartCoroutine(_StartLaunch(fTime));
    }
    IEnumerator _StartLaunch (float fTime)
    {
        objSkip.SetActive(false);
        bTimerOn = false;
        imgFill.fillAmount = 0f;
        tmp.text = "0";
        rectRotate.localEulerAngles = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(fTime+1f);

        int iRes = timer_list[UnityEngine.Random.Range(0,timer_list.Length)];
        int iProgress = 0;

        StartCoroutine(IncreaseAmount(0, iRes, 1f, (state) => {
            tmp.text = state.ToString();
            imgFill.fillAmount = (float)state / (float)iRes;
            rectRotate.localEulerAngles = new Vector3(0, 0, -360f * imgFill.fillAmount);
        }));
        //while(iProgress < iRes){
        //    iProgress++;
        //    tmp.text = iProgress.ToString();
        //    imgFill.fillAmount = (float)iProgress / (float)iRes;
        //    rectRotate.localEulerAngles = new Vector3(0, 0, -360f * imgFill.fillAmount);
        //    yield return null;
        //}
        yield return new WaitForSeconds(1f);
        gotimer(iRes);
        StartCoroutine(anim2sec());
        objSkip.SetActive(true);

    }
    void Update () {
		if(bTimerOn){
			float fTimeleft = fEnd-Time.time;
			tmp.text = fTimeleft.ToString("F0");
			imgFill.fillAmount = (fTot-fTimeleft)/fTot;
			rectRotate.localEulerAngles = new Vector3(0,0,-360f*imgFill.fillAmount);
			if(imgFill.fillAmount==1f) timerover();
		}
	}
    void gotimer ( int val ) {
		bTimerOn = false;
		bStart = false;
		fDuration = (float)val; 
		tmp.text = val.ToString();
		fTot = (float)val;
		imgFill.fillAmount=0f;
		rectRotate.localEulerAngles = new Vector3(0,0,0);
		StartCoroutine(anim2sec());
	}
	public void click () {
		if(bStart){
			bTimerOn = !bTimerOn;
			if(bTimerOn){ //end of pause
				fEnd += (Time.time-fStartPause);
			}else{
				fStartPause = Time.time;
			}
		}else{
			bStart=true;
			fEnd = Time.time + fDuration;
			bTimerOn = true;
		}
	}
	public void timerover () {
        //transform.gameObject.SetActive(false);
        bTimerOn = false;
        imgFill.fillAmount = 0f;
        tmp.text = "0";
        rectRotate.localEulerAngles = new Vector3(0, 0, 0);
        manager.TimerOver();
        objSkip.SetActive(false);
    }
	IEnumerator anim2sec () {
		float fWait = 2f;
		float fSwitch = 0.2f;
		float fEnd = Time.time+fWait;
		while(Time.time<fEnd){
			tmp.font = fontBold;
			yield return new WaitForSeconds(fSwitch);
			tmp.font = fontHighlight;
			yield return new WaitForSeconds(fSwitch);
		}
		tmp.font = fontBold;
	}
    IEnumerator IncreaseAmount(int iStart, int iEnd, float timeOfTravel, Action<int> action)
    {
        float currentTime = 0; // actual floting time
        float normalizedValue;
        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel; // we normalize our time 
            action((int)Mathf.Lerp(iStart, iEnd, normalizedValue));
            yield return null;
        }
        action(iEnd);
        action = null;
    }
}
