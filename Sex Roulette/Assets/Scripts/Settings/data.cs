﻿using System.Collections.Generic;
using UnityEngine;
using sexroulette;
using tod_class;

public class data : MonoBehaviour {

    private static List<ObjectSexGame> _objects;
    private static List<string> _categories;
    private static bool _soft;
    private static string _notificationId = "";
    private static List<string> _debugs = new List<string>();

    private static List<Player> _players = new List<Player>();
    private static List<string> sessionsList = new List<string>();

    private static string _writer_id = "";
    private static UserInfo _userInfo = null;
    private static bool _premium = false;
    private static string _price_weekly = "$4.99";
    private static float _fprice_weekly = 4.99f;
    private static string _currency = "";

    public enum GameType
    {
        Scenario, ToD, Dice, Wheel, NeverEver
    }
    public static string Split {
        get
        {
            if (PlayerPrefs.HasKey("Split_Info"))
            {
                return PlayerPrefs.GetString("Split_Info");
            }
            else
            {
                string split = UnityEngine.Random.Range(0, 100).ToString();
                PlayerPrefs.SetString("Split_Info", split);
                return split;
            }
        }
    }
    public static GroupSplit SplitGroup
    {
        get
        {
            GroupSplit group = new GroupSplit();
            int val = int.Parse(Split);
            if (val < 55)
            {
                group.name = "classic";
                group.inapp = "sexrouletteweekly1";
                group.subtitle_key = "premium/Subtitle weekly";
                group.pricing_key = "premium/Pricing weekly";
            }
            else if (val < 70)
            {
                group.name = "week_cher";
                group.inapp = "sexrouletteweekly2";
                group.subtitle_key = "premium/Subtitle weekly";
                group.pricing_key = "premium/Pricing weekly";
            }
            else if (val < 85)
            {
                group.name = "monthly";
                group.inapp = "sexroulettemonthly1";
                group.subtitle_key = "premium/Subtitle monthly";
                group.pricing_key = "premium/Pricing monthly";
            }
            else
            {
                group.name = "yearly";
                group.inapp = "sexrouletteyearly1";
                group.subtitle_key = "premium/Subtitle yearly";
                group.pricing_key = "premium/Pricing yearly";
            }
            return group;
        }
    }
    public class GroupSplit
    {
        public string name;
        public string inapp;
        public string subtitle_key;
        public string pricing_key;
    }

    public static UserInfo user_info
    {
        get
        {
            return _userInfo;
        }
        set
        {
            _userInfo = value;
        }
    }

    public static string AppName {
		get {
			return "SexRoulette";
		}
	}
	public static string AppVersion {
		get {
			#if UNITY_ANDROID
			return "6.0";
			#elif UNITY_IPHONE
			return "6.0";
			#endif
		}
    }
    public static string WriterId
    {
        get
        {
            return _writer_id;
        }
        set
        {
            _writer_id = value;
        }
    }
    public static bool Premium
    {
        get
        {
            return _premium;
        }
        set
        {
            _premium = value;
        }
    }
    public static string PriceWeekly
    {
        get
        {
            return _price_weekly;
        }
        set
        {
            _price_weekly = value;
        }
    }
    public static float fPriceWeekly
    {
        get
        {
            return _fprice_weekly;
        }
        set
        {
            _fprice_weekly = value;
        }
    }
    public static string CurrencySymbol
    {
        get
        {
            return _currency;
        }
        set
        {
            _currency = value;
        }
    }
    public static string Currency
    {
        get
        {
            return _currency;
        }
        set
        {
            _currency = value;
        }
    }

    public static bool Soft {
		get {
			return _soft;
		} set {
			_soft = value;
		}
	}
    public static string SessionsList
    {
        get
        {
            return PlayerPrefs.GetString("SessionListDate");
        }
    }
    public static string NotificationId
    {
        get
        {
            return _notificationId;
        }
        set
        {
            _notificationId = value;
        }
    }
    public static void NewSessionStarting()
    {
        sessionsList = new List<string>();
        if (PlayerPrefs.HasKey("SessionListDate"))
        {
            string str = PlayerPrefs.GetString("SessionListDate");
            str = str.Replace("{" + '"' + "array" + '"' + ":", "");
            if (str[str.Length - 1] == '}') str = str.Substring(0, str.Length - 1);
            sessionsList = JsonHelper.getJsonArray<string>(str);
        }
        sessionsList.Add(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        PlayerPrefs.SetString("SessionListDate", JsonHelper.ToJson(sessionsList));
    }

    public static List<ObjectSexGame> Objects {
		get {
			return _objects;
		} set {
			_objects = value;
		}
	}
    public static List<Player> players
    {
        get
        {
            return _players;
        }
        set
        {
            _players = value;
        }
    }
    public static void SavePlayers ()
    {
        System.IO.File.WriteAllText(Application.persistentDataPath + "/PlayerList.json", JsonHelper.ToJson(_players));
    }
    public static void LoadPlayers ()
    {
        _players = new List<Player>();
        if (System.IO.File.Exists(Application.persistentDataPath + "/PlayerList.json"))
        {
            Debug.Log("Load player");
            string strSession = System.IO.File.ReadAllText(Application.persistentDataPath + "/PlayerList.json");
            strSession = strSession.Replace("{" + '"' + "array" + '"' + ":", "");
            if (strSession[strSession.Length - 1] == '}') strSession = strSession.Substring(0, strSession.Length - 1);
            _players = JsonHelper.getJsonArray<Player>(strSession);
        }
        else
        {
            Debug.Log("No players");
        }
    }
    public static List<string> Categories {
		get {
			return _categories;
		} set {
			_categories = value;
		}
	}
	public static string AppUrl {
		get{
			#if UNITY_ANDROID
			return "https://play.google.com/store/apps/details?id=com.ultimatepartyapp.sexroulette";
			#elif UNITY_IPHONE
			return "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1093683062&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";
			#endif
		}
	}
    //public static bool PremiumSexGame
    //{
    //    get
    //    {
    //        return PlayerPrefs.HasKey("sexrouletteunlock1") || PlayerPrefs.HasKey("sexrouletteunlock2") || PlayerPrefs.HasKey("sexrouletteunlock3");
    //    }
    //}
    public static bool bScenarioCatAvailable ( int id)
    {
        if (_premium) return true;
        return PlayerPrefs.HasKey("sexrouletteunlock" + id.ToString());
    }
    public static bool bTodCatAvailable ( Category category)
    {
        if (_premium || category.free) return true;
        return PlayerPrefs.HasKey("tod4buypack" + category.old_id);
    }
    public static bool PremiumWheel {
		get
        {
			return _premium || PlayerPrefs.HasKey("wheelunlocked");
		}
	}
    public static bool PremiumNever
    {
        get
        {
            return _premium || PlayerPrefs.HasKey("neverunlocked");
        }
    }
    public static bool PremiumDice {
		get{
			return _premium || PlayerPrefs.HasKey("sexroulette_premium_dice");
		}
	}
	public static bool PremiumAllApp {
		get{
			return PlayerPrefs.HasKey("wheelunlocked") && PlayerPrefs.HasKey("sexroulette_premium_dice") && PlayerPrefs.HasKey("tod4buypack1") && PlayerPrefs.HasKey("tod4buypack2") && PlayerPrefs.HasKey("tod4buypack3") && PlayerPrefs.HasKey("tod4buypack4") && PlayerPrefs.HasKey("sexrouletteunlock1") && PlayerPrefs.HasKey("sexrouletteunlock2") && PlayerPrefs.HasKey("sexrouletteunlock3");
		}
	}
	public static List<string> Debugs {
		get {
			return _debugs;
		} set {
			_debugs = value;
		}
	}
	public static void ShowRateUsPopup ( ) {
		#if UNITY_IPHONE
		iOSReviewRequest.Request();
		#else
		string title = "";
		string msg = I2LocManager.get_text("RateUs/Catch Phrase");
        string cta1 = I2LocManager.get_text("RateUs/Yes");
        string cta2 = I2LocManager.get_text("RateUs/No");
        StanManager.ShowMessage2CTA(title, msg, cta1, cta2, () => { Application.OpenURL(AppUrl); }, () => { });
		#endif
	}

}
