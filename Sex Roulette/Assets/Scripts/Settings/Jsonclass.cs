﻿using System;
using System.Collections.Generic;
using UnityEngine;


		public class JsonHelper
		{
			
			public static List<T> getJsonArray<T>(string json)
			{
				string newJson = "{ \"array\": " + json + "}";
				Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
				return wrapper.array;
			}

			[Serializable]
			private class Wrapper<T>
			{
				public List<T> array;
			}
			
			
			
			public static string ToJson<T>(List<T> array)
			{
				Wrapper<T> wrapper = new Wrapper<T>();
				wrapper.array = array;
				return JsonUtility.ToJson(wrapper);
			}

			public static string ToJson<T>(List<T> array, bool prettyPrint)
			{
				Wrapper<T> wrapper = new Wrapper<T>();
				wrapper.array = array;
				return JsonUtility.ToJson(wrapper, prettyPrint);
			}
		}