﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Waiting : MonoBehaviour {

	private static Waiting instance;
    private Action<bool> action = null;

    void Awake () {
		DontDestroyOnLoad(transform.gameObject);
		if (FindObjectsOfType(GetType()).Length > 1){
			Destroy(gameObject);
		}else{
			instance = this;
			transform.Find("Background").Find("CancelButton").GetComponent<Button>().onClick.AddListener(() => Cancel());
            _HideWaiting();
        }
	}
	public static void ShowWaiting (Action<bool> action2 = null) {
        instance.action = action2;
        instance._ShowWaiting();
	}
	void _ShowWaiting () {
		gameObject.SetActive(true);
	}
	public static void HideWaiting () {
        instance._HideWaiting();
	}
	void _HideWaiting () {
        action = null;
		gameObject.SetActive(false);
	}
    void Cancel ()
    {
        if (action != null) action(true);
        _HideWaiting();
    }
}
