﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sexroulette {

    [System.Serializable]
    public class UserInfo
    {
        public string country, countryCode, city;
        public string status;
    }
    [System.Serializable]
    public class Player {
		public string name;
		public bool isMale;
        public bool bSexSelected = false;
        public string rdmId;
	}
	public class ObjectSexGame {
		public int id;
		public string name;
		public List<string> category;
		public string image;
		public bool selected;
	}
	[System.Serializable] 
	public class SexRouletteChallenge {
		public int id , mode, round, timer;
		public string target, challenge;
	}
}
