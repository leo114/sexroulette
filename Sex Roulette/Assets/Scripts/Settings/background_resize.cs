﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class background_resize : MonoBehaviour {
	[SerializeField] bool isPortrait;
	[SerializeField] RectTransform rectCanvas;
	void Start () {
		float width = rectCanvas.rect.width;///rectCanvas.localScale.x;
		float height = (width*512f)/288f;
		if(!isPortrait){
			height = (width*288f)/512f;
		}
		if(height<rectCanvas.rect.height){
			height = rectCanvas.rect.height;
			if(isPortrait) width = (height*288f)/512f;
			else width = (height*512f)/288f;
		}
		GetComponent<RectTransform>().sizeDelta = new Vector2(width,height);
		StartCoroutine(ieWait());
	}
	IEnumerator ieWait () {
		yield return new WaitForEndOfFrame();
		Start();
	}
}
