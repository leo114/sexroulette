﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

public class Special : MonoBehaviour {

	
	public static void SendEventDiv ( string str ){
		Debug.Log(str);
		WWWForm form = new WWWForm();
		form.AddField( "query", str);
		string url = "http://phpstack-156860-474885.cloudwaysapps.com/GTM/querypost.php";
		UnityWebRequest uwr = UnityWebRequest.Post(url,form);
		uwr.SendWebRequest();
	}
	public static IEnumerator InsertAndGetIdDiv ( string query , Action<int> action = null ) {
		Debug.Log("Insert : " + query);
		string url = "http://phpstack-156860-474885.cloudwaysapps.com/Tool/inserting.php";
		WWWForm form = new WWWForm();
		form.AddField("query",query);
		UnityWebRequest uwr = UnityWebRequest.Post(url,form);
		yield return uwr.SendWebRequest();
		if(uwr.isNetworkError || uwr.isHttpError){
			Debug.Log(uwr.error);
		}else{
			Debug.Log(uwr.downloadHandler.text);
			if(action!=null){
				int number;
				bool success = Int32.TryParse(uwr.downloadHandler.text, out number);
				if (success){
					action(int.Parse(uwr.downloadHandler.text));
				}else{
					action(0);
				}
			}
		}
	}
	public static string CreateInsertString ( string strTable , Dictionary<string,string> dic ) {
		string query = "INSERT INTO `"+strTable+"` ({Fields}) VALUES ({Values});";
		string strFields = "";
		string strValues = "";
		foreach(KeyValuePair<string,string> item in dic){
			strFields+="`"+item.Key+"`,";
			strValues+="'"+item.Value.Replace("'","''")+"',";
		}
		strFields = strFields.Substring(0, strFields.Length - 1);
		strValues = strValues.Substring(0, strValues.Length - 1);
		query = query.Replace("{Fields}",strFields).Replace("{Values}",strValues);
		return query;
	}
	public static string CreateUpdateString ( string strTable , Dictionary<string,string> dic , int id ) {
		string query = "UPDATE `"+ strTable + "` SET {Changes} WHERE id = " + id.ToString();
		string strChanges = "";
		foreach(KeyValuePair<string,string> item in dic){
			strChanges+="`"+item.Key+"`='"+item.Value.Replace("'","''")+"',";
		}
		strChanges = strChanges.Substring(0, strChanges.Length - 1);
		query = query.Replace("{Fields}",strChanges).Replace("{Changes}",strChanges);
		return query;
	}
   /*public static void IncreaseAmountForOneSignalTrigger(string str)
    {
        if (PlayerPrefs.HasKey(str))
        {
            PlayerPrefs.SetInt(str, PlayerPrefs.GetInt(str) + 1);
        }
        else
        {
            PlayerPrefs.SetInt(str, 1);
        }
        OneSignal.AddTrigger(str, PlayerPrefs.GetInt(str).ToString());
        OneSignal.SendTag(str, PlayerPrefs.GetInt(str).ToString());
    }*/
    public static void ContactUs()
    {
        string strLg = I2LocManager.get_text("Language support");
        string str = Regex.Replace(SystemInfo.deviceModel + "  " + SystemInfo.operatingSystem, "[^\\w\\._ ]", "");
        string strInfo = MyEscapeURL(str);
        string url = "https://gtmapp.com/support/new_ticket/" + strLg + "/12/" + strInfo;
        Debug.Log(url);
        OpenUrlLocally(url, "Greentomato Support");
    }
    static string MyEscapeURL(string url)
    {
        return UnityWebRequest.EscapeURL(url).Replace("+", "%20");
    }
    public static void RateUs ()
    {
        Application.OpenURL(data.AppUrl);
    }
    public static void PremiumManagerOneSignal()
    {
        if (data.Premium)
        {
            EventManager.Instance.SendEvent("VIP", "1", true, false, true, false);
            EventManager.Instance.RemoveEvent("FREE", true, true);
            EventManager.Instance.RemoveEvent("EXPIRED", true, true);
            PlayerPrefs.SetInt("WasPremium", 1);
            PlayerPrefs.SetInt("LastOpenWasPremium", 1);
        }
        else
        {
            EventManager.Instance.SendEvent("FREE", "1", true, false, true, false);
            EventManager.Instance.RemoveEvent("VIP", true, true);
            if (PlayerPrefs.HasKey("WasPremium"))
            {
                EventManager.Instance.IncreaseTrigger("EXPIRED", true, false, true, false);
                if (PlayerPrefs.HasKey("LastOpenWasPremium"))
                {
                    PlayerPrefs.DeleteKey("LastOpenWasPremium");
                    EventManager.Instance.SendEvent("JustExpired", "1", false, false, true, false);
                }
            }
        }
    }
    public static void OpenUrlLocally ( string url , string title)
    {
        InAppBrowser.DisplayOptions options = new InAppBrowser.DisplayOptions();
        options.displayURLAsPageTitle = false;
        if (!string.IsNullOrEmpty(title)) options.pageTitle = title;
        else options.pageTitle = url;
        InAppBrowser.OpenURL(url, options);
    }
}
