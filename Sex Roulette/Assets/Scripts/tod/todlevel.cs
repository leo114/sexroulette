﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using tod_class;

public class todlevel : MonoBehaviour {
	
    [SerializeField] Transform parent,child;
    [SerializeField] tod_manager manager;

    [SerializeField] GameObject objBtnNext, objWarning;


    private void Start()
    {
        foreach ( Category cat in tod_data.levels ) {
            ShowCat(cat);
            SelectCategory(cat, data.bTodCatAvailable(cat));
        }
        parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(150, 0);
        DisplayWarning();
    }
    void ShowCat ( Category category ) {
        Debug.Log(category.name);
        RectTransform rect = Instantiate(child).GetComponent<RectTransform>();
        rect.SetParent(parent);
        rect.name = category.old_id.ToString();
        rect.gameObject.SetActive(true);
        rect.localScale = new Vector3(1, 1, 1);
        rect.Find("Text").GetComponent<TextMeshProUGUI>().text = category.name;
        rect.Find("Image").Find("Icon").GetComponent<Image>().sprite = category.sprite;
        rect.Find("Lock").gameObject.SetActive(!data.bTodCatAvailable(category));
        rect.GetComponent<Button>().onClick.AddListener(() => ClickCat(category));
    }
    void ClickCat ( Category category)
    {
        if (data.bTodCatAvailable(category))
        {
            SelectCategory(category, !is_selected(category));
        }
        else
        {
            Premium.ShowPremium("TodLevel_" + category.old_id.ToString(), data.GameType.ToD);
        }
        DisplayWarning();
    }
    bool is_selected ( Category category)
    {
        return parent.Find(category.old_id.ToString()).Find("Selected").gameObject.activeInHierarchy;
    }
    void SelectCategory ( Category category, bool b)
    {
        parent.Find(category.old_id.ToString()).Find("Selected").gameObject.SetActive(b);
    }
    void DisplayWarning ()
    {
        bool bOK = false;
        foreach (Category category in tod_data.levels)
        {
            if (is_selected(category))
            {
                bOK = true;
                break;
            }
        }
        objWarning.SetActive(!bOK);
        objBtnNext.SetActive(bOK);
    }
    public void SelectAll()
    {
        foreach (Category cat in tod_data.levels)
        {
            if (data.bTodCatAvailable(cat))
            {
                SelectCategory(cat, true);
            }
            else
            {
                Premium.ShowPremium("Tod_Level_All", data.GameType.ToD);
                break;
            }
        }
    }
    public void Next ()
    {
        List<Category> categories = new List<Category>();
        foreach(Category category in tod_data.levels)
        {
            if (is_selected(category))
            {
                categories.Add(category);
            }
        }
        if (categories.Count == 0)
        {

        }
        else
        {
            manager.SelectLevel(categories);
            //GO NEXT
        }
    }
    public void RefreshLevel( bool b)
    {
        foreach (Transform item in parent)
        {
            if (item.gameObject.activeInHierarchy) Destroy(item.gameObject);
        }
        Start();
    }
}
