﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

public class tod_own : MonoBehaviour
{

    [SerializeField] tod_manager manager;
    private Transform parent, child;

    //New
    private GameObject ScreenAdd;
    private TMP_InputField field;
    private string strType = "";

    private Button btnAddPlayer;
    private Button btnSelectTruth;
    private Button btnSelectDare;
    private GameObject TruthOrDareMissing;
    private GameObject Blocker;


    [System.Serializable]
    public class UserChallenge
    {
        public int id, onn;
        public string user, language, content, type;
    }

    private void Awake()
    {
        parent = transform.Find("Popup").Find("Scroll View").Find("Viewport").Find("Content");
        child = parent.Find("Entry");
        child.gameObject.SetActive(false);
        transform.Find("Popup").Find("AddChallengeButton").GetComponent<Button>().onClick.AddListener(() => ClickAddChallenge());
        transform.Find("Popup").Find("CloseButton").GetComponent<Button>().onClick.AddListener(() => Back());

        //new
        ScreenAdd = transform.Find("New").gameObject;
        ScreenAdd.SetActive(false);
        Transform tAdd = ScreenAdd.transform;
        field = ScreenAdd.transform.Find("FieldNew").GetComponent<TMP_InputField>();
        btnAddPlayer = tAdd.Find("AddPlayerButton").GetComponent<Button>();
        btnAddPlayer.onClick.AddListener(() => AddPlayer());

        tAdd.Find("ValidButton").GetComponent<Button>().onClick.AddListener(() => ValidQuestion());
        tAdd.Find("CancelButton").GetComponent<Button>().onClick.AddListener(() => DiscardNew());
        btnSelectTruth = tAdd.Find("TruthButton").GetComponent<Button>();
        btnSelectTruth.onClick.AddListener(() => SelectTruthOrDare(true));
        btnSelectDare = tAdd.Find("DareButton").GetComponent<Button>();
        btnSelectDare.onClick.AddListener(() => SelectTruthOrDare(false));
        Blocker = tAdd.Find("Blocker").gameObject;
        Blocker.SetActive(false);
        TruthOrDareMissing = tAdd.Find("TruthOrDareMissing").gameObject;
        TruthOrDareMissing.SetActive(false);
    }
    void Start()
    {
        StartCoroutine(Loading());
    }
    void Back ()
    {
        gameObject.SetActive(false);
    }
    IEnumerator Loading()
    {
        Waiting.ShowWaiting((canceled) => {
            if (canceled == true)
            {
                Back();
            }
        });
        string query = "SELECT * FROM TODFromUser WHERE user='" + data.WriterId + "'";
        string url = "https://phpstack-156860-474885.cloudwaysapps.com/ToD/jsonselect.php";
        WWWForm form = new WWWForm();
        form.AddField("query", query);
        debugeditor(query);
        UnityWebRequest uwr = UnityWebRequest.Post(url, form);
        uwr.timeout = 6;
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            debugeditor(uwr.error);
        }
        else
        {
            List<UserChallenge> list = JsonHelper.getJsonArray<UserChallenge>(uwr.downloadHandler.text);
            foreach (UserChallenge q in list)
            {
                yield return StartCoroutine(ShowChalenge(q));
            }
        }
        Waiting.HideWaiting();
    }
    IEnumerator ShowChalenge(UserChallenge q)
    {
        RectTransform rect = Instantiate(child).GetComponent<RectTransform>();
        rect.SetParent(parent);
        rect.name = "Own_" + q.id;
        rect.localScale = new Vector3(1, 1, 1);
        rect.gameObject.SetActive(true);
        TextMeshProUGUI tmpMain = rect.Find("Text").GetComponent<TextMeshProUGUI>();
        tmpMain.text = q.content;
        rect.Find("DeleteButton").GetComponent<Button>().onClick.AddListener(() => RemoveChallenge(q, rect));
        rect.Find("TruthDareButton").GetComponent<Button>().onClick.AddListener(() => SwitchTruthOrDare(q, rect));
        rect.Find("OnOffButton").GetComponent<Button>().onClick.AddListener(() => SwitchOnOff(q, rect));
        rect.Find("EditButton").GetComponent<Button>().onClick.AddListener(() => EditChallenge(q, rect));
        ShowOnTruth(q, rect);
        yield return null;
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, tmpMain.preferredHeight + 25);
    }
    void SwitchTruthOrDare(UserChallenge q, RectTransform rect)
    {
        if (q.type == "Truth") q.type = "Dare";
        else q.type = "Truth";
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("type", q.type);
        Special.SendEventDiv(Special.CreateUpdateString("TODFromUser", dic, q.id));
        ShowOnTruth(q, rect);
    }
    void SwitchOnOff(UserChallenge q, RectTransform rect)
    {
        if (q.onn == 0) q.onn = 1;
        else q.onn = 0;
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("onn", q.onn.ToString());
        Special.SendEventDiv(Special.CreateUpdateString("TODFromUser", dic, q.id));
        ShowOnTruth(q, rect);
    }
    void ShowOnTruth(UserChallenge q, RectTransform rect)
    {
        rect.Find("OnOffButton").Find("On").gameObject.SetActive(q.onn == 1);
        rect.Find("OnOffButton").Find("Off").gameObject.SetActive(q.onn == 0);

        rect.Find("TruthDareButton").Find("Truth").gameObject.SetActive(q.type == "Truth");
        rect.Find("TruthDareButton").Find("Dare").gameObject.SetActive(q.type == "Dare");

    }
    void RemoveChallenge(UserChallenge q, RectTransform rect)
    {
        Special.SendEventDiv("DELETE FROM TODFromUser where id = " + q.id);
        Destroy(rect.gameObject);
    }


    private UserChallenge edition_challenge = null;
    private RectTransform rect_in_edition = null;
    void EditChallenge(UserChallenge q, RectTransform rect)
    { 
        edition_challenge = q;
        rect_in_edition = rect;
        field.text = q.content.Replace("<b>", "").Replace("</b>", "");
        strType = q.type;
        SelectTruthOrDare(strType == "Truth");
        btnAddPlayer.interactable = true;
        ScreenAdd.SetActive(true);
        TruthOrDareMissing.SetActive(false);
    }
    void ClickAddChallenge()
    {
        if (parent.childCount >= 4 && !data.Premium)
        {
            string _text = I2LocManager.get_text("TOD/Challenge max limit");
            string _yes = I2LocManager.get_text("TOD/Challenge max limit yes");
            string _no = I2LocManager.get_text("TOD/Challenge max limit no");
            string strPackAllTod = "$9.99";
            if (PlayerPrefs.HasKey("price_tod_pack_all")) strPackAllTod = PlayerPrefs.GetString("price_tod_pack_all");
            string strText = _text.ToString().Replace("{price}", strPackAllTod);
            StanManager.ShowMessage2CTA("", _text, _yes, _no, () => {
                //manager.BuyPack("tod4all");
                Premium.ShowPremium("Tod_Own_Screen", data.GameType.ToD);
            },
            () => {
                Debug.Log("No button pressed");
            });
            return;
        }
        Debug.Log("NEW");
        edition_challenge = null;
        strType = "";
        Color colTruthDisable = btnSelectTruth.transform.Find("Image").GetComponent<Image>().color;
        colTruthDisable.a = 0.6f;
        Color colDareDisable = btnSelectDare.transform.Find("Image").GetComponent<Image>().color;
        colDareDisable.a = 0.6f;
        btnSelectTruth.transform.Find("Image").GetComponent<Image>().color = colTruthDisable;
        btnSelectDare.transform.Find("Image").GetComponent<Image>().color = colDareDisable;
        btnAddPlayer.interactable = true;
        ScreenAdd.SetActive(true);
        TruthOrDareMissing.SetActive(false);
    }

    void AddPlayer()
    {
        string strToAdd = "";
        if (!field.text.Contains("{Player1}"))
        {
            strToAdd = "{Player1}";
        }
        else if (!field.text.Contains("{Player2}"))
        {
            strToAdd = "{Player2}";
            btnAddPlayer.interactable = false;
        }
        else
        {
            btnAddPlayer.interactable = false;
            return;
        }
        if (field.text.Length > 0)
        {
            if (field.text[field.text.Length - 1] != ' ') strToAdd = " " + strToAdd;
        }
        field.text += strToAdd;
        Invoke("selectfield", 1.5f);
    }
    void selectfield()
    {
        field.Select();
    }
    void SelectTruthOrDare(bool truth)
    {
        TruthOrDareMissing.SetActive(false);
        Color colTruthEnable = btnSelectTruth.transform.Find("Image").GetComponent<Image>().color;
        colTruthEnable.a = 1f;
        Color colTruthDisable = btnSelectTruth.transform.Find("Image").GetComponent<Image>().color;
        colTruthDisable.a = 0.6f;
        Color colDareEnable = btnSelectDare.transform.Find("Image").GetComponent<Image>().color;
        colDareEnable.a = 1f;
        Color colDareDisable = btnSelectDare.transform.Find("Image").GetComponent<Image>().color;
        colDareDisable.a = 0.6f;
        if (truth)
        {
            strType = "Truth";
            btnSelectTruth.transform.Find("Image").GetComponent<Image>().color = colTruthEnable;
            btnSelectDare.transform.Find("Image").GetComponent<Image>().color = colDareDisable;
        }
        else
        {
            strType = "Dare";
            btnSelectTruth.transform.Find("Image").GetComponent<Image>().color = colTruthDisable;
            btnSelectDare.transform.Find("Image").GetComponent<Image>().color = colDareEnable;
        }
    }
    void ValidQuestion()
    {
        string str = field.text;
        if (str.Length < 1) return;
        if (strType == "")
        {
            TruthOrDareMissing.SetActive(true);
            return;
        }
        str = str.Replace("{Player1}", "<b>{Player1}</b>");
        str = str.Replace("{Player2}", "<b>{Player2}</b>");
        Blocker.SetActive(true);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("user", data.WriterId);
        dic.Add("language", I2.Loc.LocalizationManager.CurrentLanguage);
        dic.Add("content", field.text);
        dic.Add("type", strType);
        dic.Add("onn", "1");
        if (edition_challenge == null)
        {
            string query = Special.CreateInsertString("TODFromUser", dic);
            StartCoroutine(Special.InsertAndGetIdDiv(query, (id) =>
            {
                if (id > 0)
                {
                    UserChallenge q = new UserChallenge();
                    q.id = id;
                    q.content = field.text;
                    q.onn = 1;
                    q.type = strType;
                    StartCoroutine(ShowChalenge(q));
                    DiscardNew();
                }
            }));
        }
        else
        {
            string query = Special.CreateUpdateString("TODFromUser", dic, edition_challenge.id);
            Special.SendEventDiv(query);
            edition_challenge.content = dic["content"];
            edition_challenge.onn = 1;
            edition_challenge.type = strType;
            TextMeshProUGUI tmpMain = rect_in_edition.Find("Text").GetComponent<TextMeshProUGUI>();
            tmpMain.text = edition_challenge.content;
            //yield return null;
            rect_in_edition.sizeDelta = new Vector2(rect_in_edition.sizeDelta.x, tmpMain.preferredHeight + 25);
            ShowOnTruth(edition_challenge, rect_in_edition);
            Blocker.SetActive(false);
            ScreenAdd.SetActive(false);
        }
    }

    void DiscardNew()
    {
        field.text = "";
        strType = "";
        ScreenAdd.SetActive(false);
        Blocker.SetActive(false);
    }



    void debugeditor ( string str)
    {
#if UNITY_EDITOR
        Debug.Log(str);
#endif
    }
}
