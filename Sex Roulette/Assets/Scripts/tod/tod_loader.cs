﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using tod_class;
using NeverClass;
using sexroulette;

public class tod_loader : MonoBehaviour
{
    private bool fatal_error = false;
    [SerializeField] Image progress;
    [SerializeField] RectTransform rectLips;
    [SerializeField] TextMeshProUGUI tmpProgress;
    [SerializeField] GameObject FatalError;
    [SerializeField] GameObject FatalErrorToHide;
    public string strToRemove;
    private float fNext;

    public AccountManager account;
    public OneSignalManager one_signal;

    IEnumerator Start()
    {
#if UNITY_EDITOR
        //PlayerPrefs.DeleteAll();
#endif
        WriterId();
        data.NewSessionStarting();
        tod_data.levels = new List<Category>();
        account.gameObject.SetActive(true);
        while (!account.is_init)
        {
            yield return null;
        }
        one_signal.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        StartCoroutine(_loading());
        StartCoroutine(GetUserInfo());
        StartCoroutine(ieLoadSoft());
    }
    private void Update()
    {
        rectLips.anchoredPosition = new Vector2(700f* progress.fillAmount, 0);
        tmpProgress.text = (progress.fillAmount*100f).ToString("F0") + "%";
    }
    void WriterId()
    {
        if (PlayerPrefs.HasKey("drinkinggamewriterid"))
        {
            data.WriterId = PlayerPrefs.GetString("drinkinggamewriterid");
        }
        else
        {
            string strOut = "";
            for (int i = 0; i < 19; i++) strOut += UnityEngine.Random.Range(0, 9).ToString();
            data.WriterId = strOut;
            PlayerPrefs.SetString("drinkinggamewriterid", strOut);
        }
    }
    IEnumerator _loading()
    {
        yield return StartCoroutine(LoadContentNever());
        progress.fillAmount = 0.2f;
        yield return StartCoroutine(_loading_tod());
        progress.fillAmount = 0.5f;
        if (!fatal_error)
        {
            int iCurrent = 0;
            int iMax = tod_data.levels.Count;
            foreach (Category cat in tod_data.levels)
            {
                progress.fillAmount = 0.5f + (0.5f * (float)iCurrent / (float)iMax);
                iCurrent++;
                yield return StartCoroutine(LoadIcon(cat));
                yield return new WaitForSeconds(0.05f);
            }
            progress.fillAmount = 1f;
            Resources.UnloadUnusedAssets();
            yield return new WaitForSeconds(0.2f);
            UnityEngine.SceneManagement.SceneManager.LoadScene("main");
        }
        else
        {
            FatalError.SetActive(true);
            FatalErrorToHide.SetActive(false);
        }
    }

    IEnumerator _loading_tod()
    {
        string _language = I2LocManager.get_text("Language");
        string strPlatform = "Android";
#if UNITY_IPHONE
        strPlatform = "iOS";
#endif
        WWW request;
        Hashtable headers = new Hashtable();
        byte[] postData = null;
        string url = "https://gtmcontent.com/api/tod/loader/" + data.AppName + "/" + _language + "/" + data.AppVersion + "/" + strPlatform + "/" + data.WriterId;
        //debugeditor(url);
        headers.Add("Accept", "application/json");
        headers.Add("X-UNITY-METHOD", "POST");
        request = new WWW(url, postData, headers);
        debugeditor(url);
        System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
        string callee = stackTrace.GetFrame(1).GetMethod().Name;
        while (true)
        {
            if (request.isDone)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }

        //catch proper client errors(eg. can't reach the server)
        if (!string.IsNullOrEmpty(request.error))
        {
            debugeditor(request.error);
            if (System.IO.File.Exists(Application.persistentDataPath + "/LoaderBackup.json"))
            {
                tod_data.levels = JsonHelper.getJsonArray<Category>(System.IO.File.ReadAllText(Application.persistentDataPath + "/LoaderBackup.json"));
            }
            else
            {
                fatal_error = true;
            }
        }
        else
        {
            debugeditor(request.text);
            string str = request.text.Replace(strToRemove, "");
            str = str.Substring(0, str.Length - 1);
            System.IO.File.WriteAllText(Application.persistentDataPath + "/LoaderBackup.json", str);

            try
            {
                tod_data.levels = JsonHelper.getJsonArray<Category>(str);
            }
            catch (Exception e)
            {
                fatal_error = true;
                //Debug.LogException(e, this);
            }
        }
        progress.fillAmount = 0.4f;
        if (!fatal_error)
        {
            int iCurrent = 0;
            int iMax = tod_data.levels.Count;
            foreach (Category cat in tod_data.levels)
            {
                //cat.color = Special.GetColorFromString(cat.color_code);
                progress.fillAmount = 0.4f + (0.6f * (float)iCurrent / (float)iMax);
                iCurrent++;
                yield return StartCoroutine(LoadIcon(cat));
                //yield return StartCoroutine(LoadExtraBackground(cat));
                yield return null;
            }
            //progress.fillAmount = 1f;
            //yield return new WaitForSeconds(0.2f);
            //SceneManager.LoadScene("Players");
        }
        else
        {
            Errored();
        }
    }
    void Errored()
    {
        FatalError.SetActive(true);
        FatalErrorToHide.SetActive(false);
    }

    /*IEnumerator LoadCategoryList()
    {
        LocalizedString _language = "TOD/Server Language";
        Debug.Log("Langue : " + _language);
        WWWForm form = new WWWForm();
        form.AddField("app", data.AppName);
        form.AddField("language", _language);
        UnityWebRequest uwr = UnityWebRequest.Post("https://phpstack-156860-474885.cloudwaysapps.com/2020/TruthOrDare/categories_loader.php", form);
        uwr.timeout = 5;
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
            if (System.IO.File.Exists(Application.persistentDataPath + "/Categories.json"))
            {
                tod_data.levels = JsonHelper.getJsonArray<Category>(System.IO.File.ReadAllText(Application.persistentDataPath + "/Categories.json"));
            }
            else
            {
                fatal_error = true;
            }
        }
        else
        {
            //Debug.Log(uwr.downloadHandler.text);
            System.IO.File.WriteAllText(Application.persistentDataPath + "/Categories.json", uwr.downloadHandler.text);
            tod_data.levels = JsonHelper.getJsonArray<Category>(uwr.downloadHandler.text);
        }
        Debug.Log("LIST RECEIVED");
        foreach (Category cat in tod_data.levels)
        {
            //cat.color = Special.GetColorFromString(cat.colorcode);
            cat.chalenges_dare = new List<Chalenge>();
            cat.chalenges_truth = new List<Chalenge>();
        }
    }
    IEnumerator LoadContent()
    {
        List<Chalenge> list = new List<Chalenge>();
        LocalizedString _language = "TOD/Server Language";
        Debug.Log("Langue : " + _language);
        WWWForm form = new WWWForm();
        form.AddField("app", data.AppName);
        form.AddField("version", data.AppVersion);
        string strPlatform = "1";
#if UNITY_IPHONE
        strPlatform = "0";
#endif
        form.AddField("platform", strPlatform);
        form.AddField("language", _language);
        Debug.Log("Language : " + _language);
        UnityWebRequest uwr = UnityWebRequest.Post("https://phpstack-156860-474885.cloudwaysapps.com/2020/TruthOrDare/content_loader.php", form);
        uwr.timeout = 5;
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
            if (System.IO.File.Exists(Application.persistentDataPath + "/Chalenges.json"))
            {
                list = JsonHelper.getJsonArray<Chalenge>(System.IO.File.ReadAllText(Application.persistentDataPath + "/Chalenges.json"));
            }
            else
            {
                fatal_error = true;
            }
        }
        else
        {
            Debug.Log(uwr.downloadHandler.text);
            System.IO.File.WriteAllText(Application.persistentDataPath + "/Chalenges.json", uwr.downloadHandler.text);
            list = JsonHelper.getJsonArray<Chalenge>(uwr.downloadHandler.text);
        }
        foreach (Chalenge chal in list)
        {
            foreach (Category cat in tod_data.levels)
            {
                if (cat.categoryId == chal.mode)
                {
                    if (chal.type == "Truth") cat.chalenges_truth.Add(chal);
                    else cat.chalenges_dare.Add(chal);
                }
            }
        }
    }*/
    IEnumerator LoadIcon(Category cat)
    {
        string pathImage = Application.persistentDataPath + "/" + cat.image;
        if (System.IO.File.Exists(pathImage))
        {
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            cat.sprite = Sprite.Create(load_texture, new Rect(0, 0, load_texture.width, load_texture.height), new Vector2(0, 0));
            yield return null;
        }
        else
        {
            string urlImage = "https://101fitness.s3.eu-west-2.amazonaws.com/TruthOrDare/SexRoulette/" + cat.image;
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                uwr.timeout = 3;
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    cat.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                }
            }
        }
    }
    IEnumerator LoadContentNever()
    {
        //List<NeverChallenge> list = new List<NeverChallenge>();
        string _language = I2LocManager.get_text("TOD/Server Language");
        Debug.Log("Langue : " + _language);
        WWWForm form = new WWWForm();
        form.AddField("app", data.AppName);
        form.AddField("version", data.AppVersion);
        string strPlatform = "1";
#if UNITY_IPHONE
        strPlatform = "0";
#endif
        form.AddField("platform", strPlatform);
        form.AddField("language", _language);
        Debug.Log("Language : " + _language);
        UnityWebRequest uwr = UnityWebRequest.Post("https://phpstack-156860-474885.cloudwaysapps.com/2020/Sex/nevereverloader.php", form);
        uwr.timeout = 5;
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
            if (System.IO.File.Exists(Application.persistentDataPath + "/ChalengesNever.json"))
            {
                Never_data.challenge = JsonHelper.getJsonArray<NeverChallenge>(System.IO.File.ReadAllText(Application.persistentDataPath + "/ChalengesNever.json"));
            }
            else
            {
                fatal_error = true;
            }
        }
        else
        {
            Debug.Log(uwr.downloadHandler.text);
            System.IO.File.WriteAllText(Application.persistentDataPath + "/ChalengesNever.json", uwr.downloadHandler.text);
            Never_data.challenge = JsonHelper.getJsonArray<NeverChallenge>(uwr.downloadHandler.text);
        }
        foreach (NeverChallenge chal in Never_data.challenge)
        {
            chal.bHard = chal.round >= 3;
        }
        Debug.Log("Never Ever Loaded");
    }
    public void Retry ()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Loader");
    }
    IEnumerator ieLoadSoft()
    {
        string strPlatform = "1";
#if UNITY_IPHONE
        strPlatform = "0";
#endif
        string query = "Select soft from Soft_manager where app = '" + data.AppName + "' AND version = '" + data.AppVersion + "' AND platform = " + strPlatform; //load soft question or not (on i2loc)
        string url = "https://phpstack-156860-474885.cloudwaysapps.com/ToD/getcategorycontentpost.php";
        WWWForm form = new WWWForm();
        form.AddField("query", query);
        form.AddField("rownbr", 1);

        UnityWebRequest uwr = UnityWebRequest.Post(url, form);
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
            data.Soft = false;
        }
        else
        {
            Debug.Log(uwr.downloadHandler.text);
            data.Soft = uwr.downloadHandler.text == "11";
        }
        if (data.Soft) EventManager.Instance.SendEvent("Soft", "1", true, false, true, false);
        else EventManager.Instance.SendEvent("Soft", "0", true, false, true, false);
        Debug.Log("Is s : " + data.Soft);
    }
    IEnumerator GetUserInfo()
    {
        data.Debugs.Add("Get User Info");
        string url = "https://pro.ip-api.com/json/?key=6dycleN8KWqHngZ";
        WWWForm form = new WWWForm();
        UnityWebRequest uwr = UnityWebRequest.Post(url, form);
        data.Debugs.Add("Calling");
        yield return uwr.SendWebRequest();
        data.Debugs.Add("Return");
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log("Error:" + uwr.error);
            data.Debugs.Add(uwr.downloadHandler.text);
        }
        else
        {
            Debug.Log("Result:" + uwr.downloadHandler.text);
            data.Debugs.Add(uwr.downloadHandler.text);
            try
            {
                data.user_info = JsonUtility.FromJson<UserInfo>(uwr.downloadHandler.text);
            }
            catch (Exception e)
            {
                data.Debugs.Add("User info error");
            }
            AccountManager.Instance.SetUserInfo(data.user_info.country, data.user_info.city, data.user_info.countryCode);
        }
    }
    void debugeditor ( string str)
    {
#if UNITY_EDITOR
        Debug.Log(str);
#endif
    }
}
