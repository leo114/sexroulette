﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tod_class 
{
    [System.Serializable]
    public class Category
    {
        public int id, old_id;
        public bool free;
        public string image, name, description, color_code;
        public List<Chalenge> challenges;
        public Sprite sprite;
        public Sprite sprExtraBanner = null;
        public Color color;
    }
    [System.Serializable]
    public class Chalenge
    {
        public int id, point, players_min, timer, round;
        public string title, text, p1, p2;
        public bool soft, truth;
    }
    /*[System.Serializable]
    public class Category
    {
        public int id, categoryId, ordre, activ, free;
        public string language, app, icon, title, description, information, colorcode;
        public List<Chalenge> chalenges_truth;
        public List<Chalenge> chalenges_dare;
        public Sprite sprite;
        public Sprite sprExtraBanner = null;
        public Sprite sprBackground = null;
        public Color color;
    }
    [System.Serializable]
    public class Chalenge
    {
        public int id, point, mode, player_min, timer;
        public string question, type, genderp1, genderp2;
    }*/
    [System.Serializable]
    public class cOwnQuestion
    {
        public string content;
        public int id;
        public string type;
        public string user;
        public string language;
        public int onn;
    }
}
