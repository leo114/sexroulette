﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using sexroulette;
using TMPro;

public class todplayerentry : MonoBehaviour {
	public Player myPlayer;
	[SerializeField] TMP_InputField field;
	[SerializeField] Image imgMale,imgFemale;
	private Color ColorOn,ColorOff;
	
	public void show ( Player p ){
		myPlayer = p;
		field.text = myPlayer.name;
		ColorOn = Color.white;
		ColorOff = ColorOn;
		ColorOff.a = 0f;
		changeSex(myPlayer.isMale);
	}
	public void changeName ( string str ) {
		if(str.Length == 0 ){
			Remove();
			return;
		}
		myPlayer.name = str;
	}
	public void changeSex ( bool isMale2 ) {
		myPlayer.isMale = isMale2;
		Color colTempMale = imgMale.color;
		Color colTempFemale = imgFemale.color;
		if(isMale2){
			colTempMale.a=1f;
			colTempFemale.a=0f;
			//imgMale.color = ColorOn;
			//imgFemale.color = ColorOff;
		}else{
			colTempMale.a=0f;
			colTempFemale.a=1f;
			//imgMale.color = ColorOff;
			//imgFemale.color = ColorOn;
		}
		imgMale.color = colTempMale;
		imgFemale.color = colTempFemale;
	}
	public void Remove () {
		Destroy(transform.gameObject);
	}
	
}
