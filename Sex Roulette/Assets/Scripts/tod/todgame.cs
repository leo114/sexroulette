﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using sexroulette;
using tod_class;

public class todgame : MonoBehaviour {
	[SerializeField] tod_manager manager;
	[SerializeField] Image imgFilled;
	[SerializeField] Animator animTod,animAnswer,animHotButton;
	[SerializeField] TextMeshProUGUI tmpPlayerSelected,tmpChallenges;
	[SerializeField] GameObject RateUs;
    [SerializeField] Image imgCatLogo;

	[SerializeField] List<Player> players;
	private string state = "TOD";
    List<Category> categories;

    private int iCurrentPlayer,iTotalQuestion;

    private List<UserChallenge> listOwnLeft = new List<UserChallenge>();
    private List<UserChallenge> listOwnRight = new List<UserChallenge>();
    private Player currentPlayer, secondPlayer;

    [SerializeField] Chalenge currentChallenge;

    [System.Serializable]
    public class UserChallenge
    {
        public int id, onn;
        public string user, language, content, type;
    }

    public void golevel ( List<Category> _categories ){
		categories = _categories;
		players=data.players;
        //imgCatLogo.sprite = cat.sprite;
        StartCoroutine(ieLoadOwnChallenge());
        iCurrentPlayer = -1;
        NextPlayer();

        tmpChallenges.text = "";
        tmpPlayerSelected.text = currentPlayer.name;
        state = "TOD";
        EventManager.Instance.IncreaseTrigger("open_tod", true, true, true, false);
        EventManager.Instance.IncreaseTrigger("scene_game_opened", true, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("open_tod");
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("scene_game_opened");
    }
	void NextPlayer ()
    {
        Debug.Log("Next player " + iCurrentPlayer.ToString());
        iCurrentPlayer++;
        if (iCurrentPlayer >= players.Count) iCurrentPlayer = 0;
        Debug.Log("New player " + iCurrentPlayer.ToString());
        currentPlayer = players[iCurrentPlayer];
        int iOtherPlayer = iCurrentPlayer;
        while (iOtherPlayer == iCurrentPlayer) iOtherPlayer = Random.Range(0, players.Count);
        secondPlayer = players[iOtherPlayer];
    }


    public void clickToD ( bool isLeft ) {
		if(state!="TOD") return;
        string _type = I2LocManager.get_text("TOD/server left");
        if (isLeft) StartCoroutine(ieAnimSwitch(1f));
		else StartCoroutine(ieAnimSwitch(0f));
		Chalenge q = new Chalenge();
        if (iTotalQuestion == 13 && !PlayerPrefs.HasKey("sexrouletterated"))
        { //rate us challenge
            string _MyLocalizedString = I2LocManager.get_text("TOD/chalengeratedare" + UnityEngine.Random.Range(0, 9).ToString());
            q.text = _MyLocalizedString;
            q.id = -1;
            RateUs.SetActive(true);
        }
        else
        { //classic
            RateUs.SetActive(false);
            if (!isLeft) _type = "TOD/server right";
            q = getNextChalenge(categories[UnityEngine.Random.Range(0, categories.Count)], _type);
            //OWN CHALLENGE
            if (iTotalQuestion > 0 && iTotalQuestion % 5 == 0)
            {
                Debug.Log("SEARCH FOR OWN QUESTION");
                if (isLeft)
                {
                    if (listOwnLeft.Count > 0)
                    {
                        int id = UnityEngine.Random.Range(0, listOwnLeft.Count);
                        q.id = -1;
                        q.text = listOwnLeft[id].content;
                        listOwnLeft.RemoveAt(id);
                    }
                }
                else
                {
                    if (listOwnRight.Count > 0)
                    {
                        int id = UnityEngine.Random.Range(0, listOwnRight.Count);
                        q.id = -1;
                        q.text = listOwnRight[id].content;
                        listOwnRight.RemoveAt(id);
                    }
                }
            }
        }
		string strChalenge = q.text;
		strChalenge = strChalenge.Replace("{Player1}", currentPlayer.name);
		strChalenge = strChalenge.Replace("{Player2}",secondPlayer.name);
		tmpChallenges.text = strChalenge;
	}
    Chalenge getNextChalenge(Category cat, string type)
    {
        bool bTempSoft = false;
        string _language = I2LocManager.get_text("Language");
        if (_language == "English")
        {
            bTempSoft = data.Soft;
        }

        List<int> idsSeen = new List<int>();
        bool is_truth = (type == "Truth");

        if (PlayerPrefs.HasKey("Ids_Seen_" + cat.id.ToString()))
        {
            string str = PlayerPrefs.GetString("Ids_Seen_" + cat.id.ToString());
            str = str.Replace("{" + '"' + "array" + '"' + ":", "");
            if (str[str.Length - 1] == '}') str = str.Substring(0, str.Length - 1);
            idsSeen = JsonHelper.getJsonArray<int>(str);
        }

        currentChallenge = null;
        int iCount = 0;
        while (iCount < 200 && currentChallenge == null)
        {
            Chalenge c = cat.challenges[UnityEngine.Random.Range(0, cat.challenges.Count)];
            if (!idsSeen.Contains(c.id) && c.soft == bTempSoft && questionIsOk(cat, c, is_truth, currentPlayer, secondPlayer))
            {
                if (cat.old_id == 3) //couple round system
                {
                    int iRound = 1;
                    if (iTotalQuestion < 6) iRound = 1;
                    else if (iTotalQuestion < 11) iRound = 2;
                    else if (iTotalQuestion < 16) iRound = 3;
                    else if (iTotalQuestion < 21) iRound = 4;
                    else iRound = 5;

                    if (iRound == c.round) currentChallenge = c;
                }
                else
                {
                    currentChallenge = c;
                }
            }
            iCount++;
        }
        if (currentChallenge == null)
        {
            idsSeen = new List<int>();
            currentChallenge = cat.challenges[UnityEngine.Random.Range(0, cat.challenges.Count)];
        }
        idsSeen.Add(currentChallenge.id);
        PlayerPrefs.SetString("Ids_Seen_" + cat.id.ToString(), JsonHelper.ToJson<int>(idsSeen));
        return currentChallenge;
    }

    bool questionIsOk(Category cat, Chalenge q, bool is_truth , Player p1, Player p2)
    {
        if (q.id == -1) return true; //if i2loc content, always ok
        if (q.players_min > data.players.Count) return false;
        if (q.truth != is_truth) return false;
        if (q.p1 == "M" && !p1.isMale) return false;
        if (q.p1 == "W" && p1.isMale) return false;
        if (q.p2 == "M" && !p2.isMale) return false;
        if (q.p2 == "W" && p2.isMale) return false;
        return true;
    }


    IEnumerator ieAnimSwitch ( float fval ) {
		state = "TransiToAnswer";
		float fAdd = 1f;
		animTod.SetTrigger("gooff");
		animAnswer.SetTrigger("goon");
		if(fval==0f) fAdd = -1f;
		while(imgFilled.fillAmount!=fval){
			imgFilled.fillAmount += fAdd*Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		state = "Answer";
	}
	public void clickOk () {
		if(state!="Answer") return;

        iTotalQuestion++;
        EventManager.Instance.IncreaseTrigger("Tod_challenge_shown", false, true, true, false);
        EventManager.Instance.SendEvent("Tod_In_Progress", iTotalQuestion.ToString(), true, true,false, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("Tod_challenge_shown");
        RateUs.SetActive(false);
		#if UNITY_EDITOR
		if(iTotalQuestion>=10 && !data.PremiumAllApp){
#else
			if(iTotalQuestion>=25 && !data.PremiumAllApp){
#endif
            iTotalQuestion = 0;
			manager.GameOver();
			return;
		}
        if (iTotalQuestion%4==1) animHotButton.SetTrigger("anim");
		StartCoroutine(ieBackToD());
        NextPlayer();
		tmpPlayerSelected.text = players[iCurrentPlayer].name;
	}
	IEnumerator ieBackToD () {
		state = "TransiToTOD";
		animTod.SetTrigger("goon");
		animAnswer.SetTrigger("gooff");
		float fAdd = 2f;
		if(imgFilled.fillAmount==1f) fAdd = -2f;
		Debug.Log("Start " + imgFilled.fillAmount + " power : " + fAdd);
		while((fAdd<0f && imgFilled.fillAmount>=0.5f) || (fAdd>0f && imgFilled.fillAmount<=0.5f)){
			//Debug.Log("Before : " + imgFilled.fillAmount);
			imgFilled.fillAmount += fAdd*Time.deltaTime;
		    //Debug.Log("After : " + imgFilled.fillAmount);
			yield return new WaitForEndOfFrame();
		}
		imgFilled.fillAmount=0.5f;
		state = "TOD";
	}
	public void GoRateUs () {
		PlayerPrefs.SetInt("sexrouletterated",1);
		data.ShowRateUsPopup();
	}
	void debugeditor ( string str ) { //send the debug.log event only if user in on Unity
		#if UNITY_EDITOR
		Debug.Log(str);
		#endif
	}

    IEnumerator ieLoadOwnChallenge()
    {
        //MY OWN QUESTIONS
        listOwnLeft = new List<UserChallenge>();
        listOwnRight = new List<UserChallenge>();
        string _MyLocalizedString = I2LocManager.get_text("TOD/server left");
        string query = "SELECT * FROM TODFromUser WHERE onn = 1 AND user='" + data.WriterId + "'";
        string url = "https://phpstack-156860-474885.cloudwaysapps.com/DG/jsonselect.php";
        WWWForm form = new WWWForm();
        form.AddField("query", query);
        debugeditor(query);
        UnityWebRequest uwr = UnityWebRequest.Post(url, form);
        uwr.timeout = 8;
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            debugeditor(uwr.error);
        }
        else
        {
            List<UserChallenge> list = JsonHelper.getJsonArray<UserChallenge>(uwr.downloadHandler.text);
            foreach (UserChallenge q in list)
            {
                if (_MyLocalizedString == q.type) listOwnLeft.Add(q);
                else listOwnRight.Add(q);
            }
            //OneSignal.SendTag("OwnQuestionAmount", list.Count.ToString());
        }
    }
}
