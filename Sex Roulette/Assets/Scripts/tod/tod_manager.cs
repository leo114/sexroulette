﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using tod_class;

public class tod_manager : MonoBehaviour {
	[SerializeField] GameObject CanvasLevel,CanvasGame;

    [SerializeField] GameObject objOwnChallenge;
    [SerializeField] Image[] imgTod;

    private void OnDestroy()
    {
        IAPManager.PremiumUnlockedEvent -= UnlockedPremium;
    }
    void Awake()
    {
        IAPManager.PremiumUnlockedEvent += UnlockedPremium;
    }
    void Start () {
        CanvasGame.SetActive(false);
		StartCoroutine(ieLoadImage());
    }
	public void clickBack () {
        Destroy(gameObject);
    }
	
	public void ClickBackFromMode () {
        Destroy(gameObject);
    }
	public void SelectLevel ( List<Category> categories ) {
		CanvasLevel.SetActive(false);
		CanvasGame.SetActive(true);
		CanvasGame.GetComponent<todgame>().golevel(categories);
	}
	public void ClickBackFromGame () {
		CanvasGame.SetActive(false);
		CanvasLevel.SetActive(true);
	}
	public void GameOver () {
        EventManager.Instance.IncreaseTrigger("GameOver", true, true, true, false);
        EventManager.Instance.IncreaseTrigger("GameOverTod", true, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("GameOver");
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("GameOverTod");
        CanvasGame.SetActive(false);
		CanvasLevel.SetActive(true);
        Premium.ShowPremium("Tod_GameOver", data.GameType.ToD);
	}
    public void GoOwnChallenge ()
    {
        objOwnChallenge.SetActive(true);
    }
	void UnlockedPremium( bool b) {
        CanvasGame.SetActive(false);
        CanvasLevel.SetActive(true);
        CanvasLevel.GetComponent<todlevel>().RefreshLevel(true);
    }
	IEnumerator ieLoadImage() {
		string _MyLocalizedString = I2LocManager.get_text("TOD/logo");
        Sprite spr = Resources.Load<Sprite>("logos/tod_en"); //+_MyLocalizedString.ToString().Replace(".png",""));
		foreach(Image img in imgTod) img.sprite = spr;

        string pathImage = Application.persistentDataPath + "/" + _MyLocalizedString;
        if (System.IO.File.Exists(pathImage))
        { //if the image is saved on the device
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            spr = Sprite.Create(load_texture, new Rect(0, 0, load_texture.width, load_texture.height), new Vector2(0, 0));
        }
        else
        { //if the image is not saved on the device, download it from the server
            string urlImage = "https://d2eeazcf1if3qf.cloudfront.net/sexroulette/" + _MyLocalizedString;
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    spr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                }
            }
        }
        if (spr!=null){
			foreach(Image img in imgTod) img.sprite = spr;
		}
	} 
}
