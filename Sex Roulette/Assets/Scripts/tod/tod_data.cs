﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using tod_class;

public class tod_data : MonoBehaviour
{
    private static List<Category> _levels;


    public static List<Category> levels
    {
        get
        {
            return _levels;
        }
        set
        {
            _levels = value;
        }
    }

}
