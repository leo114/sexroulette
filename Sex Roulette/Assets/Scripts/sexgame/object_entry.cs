﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using sexroulette;

public class object_entry : MonoBehaviour {
	private ObjectSexGame item;
    [SerializeField] Color colCircleNonSelected;
    [SerializeField] Color colLogoSelected;
    [SerializeField] Color colorTextOn,colorTextOff;
	
	public void show ( ObjectSexGame item2 ) { //object information
		item = item2;
        GetComponent<Button>().onClick.AddListener(() =>  Click() );
        if (PlayerPrefs.HasKey("object_activity_" + item.id.ToString())){
            item.selected = PlayerPrefs.GetInt("object_activity_" + item.id.ToString()) == 1;
        }
        else
        {
            item.selected = false;
        }
        transform.Find("Background").Find("Logo").GetComponent<Image>().sprite = Resources.Load<Sprite>("objects/"+item.image); //load correct sprite and display
        transform.Find("Name").GetComponent<TextMeshProUGUI>().text = item.name; //display the name
        DisplayState();
    }
    void Click ()
    {
        item.selected = !item.selected;
        DisplayState();
    }
    void DisplayState ()
    {
        if (item.selected)
        {
            transform.Find("Background").Find("InsideLight").GetComponent<Image>().color = Color.white;
            transform.Find("Background").Find("Logo").GetComponent<Image>().color = colLogoSelected;
            transform.Find("Name").GetComponent<TextMeshProUGUI>().color = Color.white;
            PlayerPrefs.SetInt("object_activity_" + item.id.ToString(), 1); //save "active"
        }
        else
        {
            transform.Find("Background").Find("InsideLight").GetComponent<Image>().color = colCircleNonSelected;
            transform.Find("Background").Find("Logo").GetComponent<Image>().color = Color.white;
            transform.Find("Name").GetComponent<TextMeshProUGUI>().color = colCircleNonSelected;
            PlayerPrefs.SetInt("object_activity_" + item.id.ToString(), 0); //save "inactive"
        }
    }

}
