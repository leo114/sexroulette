﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using sexroulette;

public class SexGameManager : MonoBehaviour {
	
	List<int> listObjectId = new List<int>();
	[SerializeField] GameObject prefabObjectSelection,SelectMode,GameScreen;
	private int iCatSelected;
	[SerializeField] Image[] imgTod;
	void Start () {
		data.Objects = new List<ObjectSexGame>();
		data.Categories = new List<string>();
		string _MyLocalizedString = I2LocManager.get_text("Objects/number"); //get the number of active object on the i2loc
		int iMax = int.Parse(_MyLocalizedString); //number of active object
		for ( int i = 0 ; i < iMax ; i++ ){ //instantiate the correct number of object
			ObjectSexGame newObj = new ObjectSexGame();
			newObj.id = i;
			_MyLocalizedString = I2LocManager.get_text("Objects/"+i.ToString()+"/name");
			newObj.name = _MyLocalizedString;
			_MyLocalizedString = I2LocManager.get_text("Objects/"+i.ToString()+"/category");
			newObj.category = new List<string>();
			string[] split = _MyLocalizedString.ToString().Split("/"[0]);
			for ( int k = 0 ; k < split.Length ; k++ ){
				newObj.category.Add(split[k]);
				if(!data.Categories.Contains(split[k])) data.Categories.Add(split[k]);
			}
			_MyLocalizedString = I2LocManager.get_text("Objects/"+i.ToString()+"/icon");
			newObj.image = _MyLocalizedString;
			newObj.selected = PlayerPrefs.GetInt("object_activity_"+i.ToString())==1;
			data.Objects.Add(newObj);
		}

        GameObject objtemp = Instantiate(prefabObjectSelection) as GameObject;
        objtemp.GetComponent<object_manager>().manager = this;
        objtemp.transform.Find("logo").GetComponent<Image>().sprite = imgTod[0].sprite;

        StartCoroutine(ieLoadImage());
        EventManager.Instance.IncreaseTrigger("open_scenario", true, true, true, false);
        EventManager.Instance.IncreaseTrigger("scene_game_opened", true, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("open_scenario");
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("scene_game_opened");
    }
	public void selectMode ( int val ) {
		iCatSelected=val;
		SelectMode.SetActive(false);
		StartGame();
	}
	public void clickPlus () {
		SelectMode.SetActive(true);
		GameScreen.SetActive(false);
	}
	public void clickBack () {
        Destroy(gameObject);
    }
	public void BackFromObjectSelection () {
		SelectMode.SetActive(true);
	}
	public void StartGame () {
		GameScreen.SetActive(true);
		GameScreen.GetComponent<sexgame_game>().show(iCatSelected,listObjectId);
	}
	IEnumerator ieLoadImage() {
		string _MyLocalizedString = I2LocManager.get_text("Sex Game/logo");
        Sprite spr = Resources.Load<Sprite>("logos/sex_en");// + _MyLocalizedString.ToString().Replace(".png",""));
		foreach(Image img in imgTod) img.sprite = spr;

        string pathImage = Application.persistentDataPath + "/" + _MyLocalizedString;
        if (System.IO.File.Exists(pathImage))
        { //if the image is saved on the device
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            spr = Sprite.Create(load_texture, new Rect(0, 0, load_texture.width, load_texture.height), new Vector2(0, 0));
        }
        else
        { //if the image is not saved on the device, download it from the server
            string urlImage = "https://d2eeazcf1if3qf.cloudfront.net/sexroulette/" + _MyLocalizedString;
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    spr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                }
            }
        }
        if (spr!=null){
			foreach(Image img in imgTod) img.sprite = spr;
		}
	} 
}
