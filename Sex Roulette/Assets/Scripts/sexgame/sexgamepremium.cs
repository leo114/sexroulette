﻿using UnityEngine;
using TMPro;

public class sexgamepremium : MonoBehaviour {
	[SerializeField] TextMeshProUGUI tmpPrice1,tmpPrice2,tmpPrice3,tmpPriceAllCat,tmpPriceAllApp;
	[SerializeField] GameObject[] objsCat;
    [SerializeField] GameObject[] objsCatOff;
    [SerializeField] sexgame_game game;
    [SerializeField] SexGameManager manager;
	private int icat;

	public void show ( int val ) {
		icat = val;
		for ( int i = 0 ; i < objsCat.Length ; i++ ) objsCat[i].SetActive(false);
        for (int i = 0; i < objsCatOff.Length; i++) objsCatOff[i].SetActive(true);
        objsCat[val].SetActive(true);
        objsCatOff[val].SetActive(false);
        string strPrice1 = "$3.99";
		string strPrice2 = "$3.99";
		string strPrice3 = "$3.99";
		string strPriceAllCat = "$7.99";
		string strPriceAllApp = "$18.99";
		if(PlayerPrefs.HasKey("pricesexgame1")) strPrice1 = PlayerPrefs.GetString("pricesexgame1");
		if(PlayerPrefs.HasKey("pricesexgame2")) strPrice2 = PlayerPrefs.GetString("pricesexgame2");
		if(PlayerPrefs.HasKey("pricesexgame3")) strPrice3 = PlayerPrefs.GetString("pricesexgame3");
		if(PlayerPrefs.HasKey("pricesexgameall")) strPriceAllCat = PlayerPrefs.GetString("pricesexgameall");
		if(PlayerPrefs.HasKey("priceAllApp")) strPriceAllApp = PlayerPrefs.GetString("priceAllApp");
		tmpPrice1.text = strPrice1;
		tmpPrice2.text = strPrice2;
		tmpPrice3.text = strPrice3;
		tmpPriceAllCat.text = strPriceAllCat;
		tmpPriceAllApp.text = strPriceAllApp;
	}
	public void clickPurchase ( string str ) {
        IAPManager.PurchasePack(str, "SexGame");
	}
    public void ClickBackManually ()
    {
        EventManager.Instance.IncreaseTrigger("Closed_Premium_Sex", true, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("Closed_Premium_Sex");
        manager.clickBack();
    }
    void Update () {
		if(PlayerPrefs.HasKey("sexrouletteunlock"+icat.ToString())){
			transform.gameObject.SetActive(false);
		}
	}
}
