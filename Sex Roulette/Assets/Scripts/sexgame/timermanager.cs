﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class timermanager : MonoBehaviour {
	[SerializeField] sexgame_game sexgamemanager;
	[SerializeField] TextMeshProUGUI tmpTime;
	[SerializeField] Image imgFill;
	[SerializeField] RectTransform rectRotate;
	[SerializeField] TMP_FontAsset fontBold,fontHighlight;
	private float fOver,fTime,fStartPause;
	public bool bStart = false;
	public bool bTimerOn = false;
	private bool bTimerActif = false;
	void OnEnable () {
		if(sexgamemanager.currentChallenge.timer>0) goTimer(sexgamemanager.currentChallenge.timer);
		else this.gameObject.SetActive(false);
		imgFill.fillAmount = 0;
		rectRotate.localEulerAngles = new Vector3(0,0,0);
	}
	public void goTimer ( int val ) {
		bTimerActif = true;
		GetComponent<RectTransform>().localScale= new Vector3(1,1,1);
		fTime = (float)val;
		bStart = false;
		tmpTime.text = fTime.ToString("F0");
		StartCoroutine(anim2sec());
	}
	void Update () {
		if(bStart && bTimerOn && bTimerActif){
			float fDiff = fOver-Time.time;
			tmpTime.text = fDiff.ToString("F0");
			imgFill.fillAmount = (fTime-fDiff)/fTime;
			rectRotate.localEulerAngles = new Vector3(0,0,-360f*imgFill.fillAmount);
			if(fDiff<0.25f) timeover();
		}
	}
	public void timeover ( ) {
		bTimerActif = false;
		bStart = false;
		bTimerOn = false;
		sexgamemanager.gonextQuestion();
		//gameObject.SetActive(false);
		GetComponent<RectTransform>().localScale= new Vector3(0,0,0);
	}
	public void click () {
		if(!bTimerActif) return;
		Debug.Log("Click timer");
		if(!bStart){
			bTimerOn = true;
			bStart=true;
			fOver = Time.time+fTime;
		}else{
			bTimerOn = !bTimerOn;
			if(bTimerOn){ //end of pause
				fOver += (Time.time-fStartPause);
			}else{
				fStartPause = Time.time;
			}
		}
	}
	IEnumerator anim2sec () {
		float fWait = 2f;
		float fSwitch = 0.2f;
		float fEnd = Time.time+fWait;
		while(Time.time<fEnd){
			tmpTime.font = fontBold;
			yield return new WaitForSeconds(fSwitch);
			tmpTime.font = fontHighlight;
			yield return new WaitForSeconds(fSwitch);
		}
		tmpTime.font = fontBold;
	}
}
