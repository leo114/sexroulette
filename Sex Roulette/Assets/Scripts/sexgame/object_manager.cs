﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using sexroulette;

public class object_manager : MonoBehaviour {
	public SexGameManager manager;
    [SerializeField] Image[] logos;
    private Transform child;
	[SerializeField] Transform parent;

    private void Awake()
    {
        child = parent.Find("Entry");
        child.gameObject.SetActive(false);
    }
    void Start () {
        StartCoroutine(ieLoadImage());
        foreach (ObjectSexGame item in data.Objects){
            InstantObject(item);
		}
	}
    void InstantObject ( ObjectSexGame obj)
    {
        RectTransform rect = Instantiate(child).GetComponent<RectTransform>();
        rect.transform.SetParent(parent);
        rect.localScale = new Vector3(1, 1, 1);
        rect.gameObject.SetActive(true);
        rect.GetComponent<object_entry>().show(obj);
    }
    public void ClickArrow ( bool bRight)
    {
        Vector3 pos = parent.position;
        if (bRight) pos.x -= 675;
        else pos.x += 675;
        parent.position = pos;
    }
    public void clicknext () {
		Destroy(transform.gameObject);
	}
	public void clickback () {
		Destroy(transform.gameObject);
	}


    IEnumerator ieLoadImage()
    {
        string _MyLocalizedString = I2LocManager.get_text("Sex Game/logo");
        Sprite spr = Resources.Load<Sprite>("logos/sex_en");// + _MyLocalizedString.ToString().Replace(".png",""));
        foreach (Image img in logos) img.sprite = spr;

        string pathImage = Application.persistentDataPath + "/" + _MyLocalizedString;
        if (System.IO.File.Exists(pathImage))
        { //if the image is saved on the device
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(pathImage);
            Texture2D load_texture;
            load_texture = new Texture2D(1, 1);
            load_texture.LoadImage(bytes);
            spr = Sprite.Create(load_texture, new Rect(0, 0, load_texture.width, load_texture.height), new Vector2(0, 0));
        }
        else
        { //if the image is not saved on the device, download it from the server
            string urlImage = "https://d2eeazcf1if3qf.cloudfront.net/sexroulette/" + _MyLocalizedString;
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(urlImage))
            {
                yield return uwr.SendWebRequest();
                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    System.IO.File.WriteAllBytes(pathImage, uwr.downloadHandler.data);
                    spr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                }
            }
        }
        if (spr != null)
        {
            foreach (Image img in logos) img.sprite = spr;
        }
    }
}
