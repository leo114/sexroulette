﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using sexroulette;
using TMPro;

public class sexgame_game : MonoBehaviour {
    [SerializeField] SexGameManager manager;
    [SerializeField] timermanager timer;
    [SerializeField] GameObject GameOverPanel;
    [SerializeField] GameObject Tips;
    [SerializeField] GameObject objBtnHot;
    public int iTotQuestion;
    private int iStep,iQuestionInStep;
    private int iCategory = -1;
    [SerializeField] Animator anim;
    [SerializeField] GameObject NextQuestionButton,LikeDislike,LoadBlock;
    [SerializeField] TextMeshProUGUI tmpPlayerName,tmpContent,tmpSpin;
    private bool bWaitForEndSpin;
    [SerializeField] Image imgThermo;
    [SerializeField] RectTransform rectThermoIcon;
    
    private int[] iListQuestionNumber = {2,2,3,2,3,2,4,6,1};
    Player p1,p2;
    [SerializeField] Sprite[] sprWheel;
    [SerializeField] Image imgWheel;
    [SerializeField] private List<SexRouletteChallenge> challenges;
    [SerializeField] public SexRouletteChallenge currentChallenge;


    private void OnDestroy()
    {
        IAPManager.PremiumUnlockedEvent -= UnlockedPremium;
    }
    void UnlockedPremium( bool b)
    {
        Debug.Log("Hmm");
        objBtnHot.SetActive(false);
//#if UNITY_IPHONE
//        NextQuestionButton.SetActive(true);
//        LoadBlock.SetActive(false);
//#endif
    }
    void Awake ()
    {
        IAPManager.PremiumUnlockedEvent += UnlockedPremium;
        if (data.players.Count >= 3)
        {
            for (int i = 0; i < iListQuestionNumber.Length; i++ ) iListQuestionNumber[i]++;
        }
        NextQuestionButton.SetActive(false);
        LikeDislike.SetActive(false);
        foreach(string str in data.Categories) Debug.Log("Cat : " + str);
        Debug.Log("Object : ");
    }
    IEnumerator LoadContent () {
        Waiting.ShowWaiting((cancel)=> {
            StopAllCoroutines();
            manager.clickBack();
        });
        string strSoft = "0";
        string _MyLocalizedStringLanguage = I2LocManager.get_text("Language");
        if (_MyLocalizedStringLanguage == "English")
        {
            if (data.Soft) strSoft = "1";
        }
        string url = "https://phpstack-156860-474885.cloudwaysapps.com/DG/jsonselect.php";
        string query = "select id,mode,round,timer,target,challenge from SexRouletteGame where lg='"+_MyLocalizedStringLanguage+"' AND mode = "+iCategory.ToString()+" AND soft = " + strSoft + " ORDER BY RAND()";
        Debug.Log(query);
        WWWForm form = new WWWForm();
        form.AddField("query",query);
        UnityWebRequest uwr = UnityWebRequest.Post(url,form);
        uwr.timeout = 7;
        yield return uwr.SendWebRequest();
        if(uwr.isNetworkError || uwr.isHttpError){
            Debug.Log(uwr.error);
        }else{
            Debug.Log("Sessions result : " + uwr.downloadHandler.text);
            challenges = JsonHelper.getJsonArray<SexRouletteChallenge>(uwr.downloadHandler.text);
            Debug.Log("Challenghe count : " + challenges.Count);
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("result")){
                gonextQuestion();
            }
        }
        Waiting.HideWaiting();

        objBtnHot.SetActive(!data.bScenarioCatAvailable(iCategory));

        if (challenges.Count<10) FailLoad();
    }
    void OnDisable () {
        imgWheel.GetComponent<RectTransform>().localEulerAngles = new Vector3(0,0,0);
    }
    void Update () {
        if(bWaitForEndSpin){
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("result")) endspin();
        }
    }
    public void show ( int icat , List<int> listObjectId ) {
        string _MyLocalizedString = I2LocManager.get_text("Spin");
        tmpSpin.text = _MyLocalizedString;
        bool bSameThanBefore = icat==iCategory;
        if(icat!=iCategory){
            iStep = 0;
            iQuestionInStep = 0;
            iTotQuestion = 0;
            imgThermo.fillAmount=0;
            rectThermoIcon.anchorMin = new Vector2(0.5f, imgThermo.fillAmount);
            rectThermoIcon.anchorMax = new Vector2(0.5f, imgThermo.fillAmount);
            rectThermoIcon.anchoredPosition = new Vector2(0, 0);
            imgWheel.sprite = sprWheel[icat];
            p1 = data.players[UnityEngine.Random.Range(0,data.players.Count)];
            p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
            while (p1 == p2) p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        }
        bWaitForEndSpin = false;
        iCategory = icat;
        StartCoroutine(LoadContent());
        NextQuestionButton.SetActive(false);
        LikeDislike.SetActive(false);
        tmpPlayerName.text = p1.name;
        if (data.bScenarioCatAvailable(iCategory))
        {

        }
        else
        {
            Premium.ShowPremium("Sex_Before_" + iCategory.ToString(), data.GameType.Scenario, 1);
        }
    }
    
    
    public void ClickSpin () {
        if(!anim.GetCurrentAnimatorStateInfo(0).IsName("waitspin")) return;
        EventManager.Instance.IncreaseTrigger("Sex_challenge_shown", false, true, true, false);
        //OneSignalInit.IncreaseAmountForOneSignalTrigger("Sex_challenge_shown");
        LoadBlock.SetActive(true);
        Invoke("RemoveLoadBlock",1f);
        tmpSpin.text = "";
        anim.SetTrigger("clickspin");
        bWaitForEndSpin=true;
        SelectQuestion();
        objBtnHot.SetActive(!data.bScenarioCatAvailable(iCategory));
    }
    void RemoveLoadBlock () {
        LoadBlock.SetActive(false);
    }
    void SelectQuestion () {
        currentChallenge = getNextCHallenge();
        if(currentChallenge==null){
            Debug.Log("Reset list");
            ResetListDone();
            currentChallenge = getNextCHallenge();
        }
        Debug.Log("Challenge selected : " + currentChallenge.id);
        SaveListDone(currentChallenge.id);
        tmpPlayerName.text = p1.name;
        string str = currentChallenge.challenge.Replace("{Player1_Allsex}",p1.name);
        str = str.Replace("{Player1_Men}",p1.name);
        str = str.Replace("{Player1_Women}",p1.name);
        str = str.Replace("{Player2_Allsex}",p2.name);
        str = str.Replace("{Player2_Men}",p2.name);
        str = str.Replace("{Player2_Women}",p2.name);
        foreach(string cat in data.Categories){
            if(str.Contains("{"+cat+"}")){
                Debug.Log("Chalenge contain " + cat);
                List<string> list = new List<string>();
                foreach(ObjectSexGame obj in data.Objects ){
                    if(obj.selected && obj.category.Contains(cat)) list.Add(obj.name);
                }
                str = str.Replace("{"+cat+"}",list[UnityEngine.Random.Range(0,list.Count)]);
            }
        }
        tmpContent.text = str;
    }
    SexRouletteChallenge getNextCHallenge () {
        List<int> listDone = new List<int>();
        if(PlayerPrefs.HasKey("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString())){
            Debug.Log("CHallenge done : " + PlayerPrefs.GetString("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString()));
            string[] split = PlayerPrefs.GetString("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString()).Split("/"[0]);
            foreach(string str in split ) listDone.Add(int.Parse(str));
        }
        foreach(SexRouletteChallenge c in challenges ){
            if(c.round == iStep){
                //Round ok
                if(!listDone.Contains(c.id)){
                    //ID OK
                    if(c.target=="B" || (c.target=="M" && p1.isMale) || (c.target=="W" && !p1.isMale) ){
                        //Sex ok
                        bool bP2Wrong = (c.challenge.Contains("Player2_Women") && p2.isMale) || (c.challenge.Contains("Player2_Men") && !p2.isMale);
                        if(!bP2Wrong){
                            //P2 sex ok
                            bool bObjectOk = true;
                            foreach(string cat in data.Categories){
                                if(c.challenge.Contains("{"+cat+"}")){
                                    bObjectOk = false;
                                    foreach(ObjectSexGame obj in data.Objects){
                                        if(obj.selected && obj.category.Contains(cat)) bObjectOk = true;
                                        //if(!bSelected) bObjectOk=false;
                                    }
                                }
                            }
                            if(bObjectOk){
                                //Object ok - All good
                                return c;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    void ResetListDone () {
        Debug.Log("RESET LIST DONE ");
        PlayerPrefs.DeleteKey("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString());
    }
    void SaveListDone ( int id ) {
        if(PlayerPrefs.HasKey("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString())){
            string str = PlayerPrefs.GetString("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString());
            PlayerPrefs.SetString("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString(),str+"/"+id.ToString());
        }else{
            PlayerPrefs.SetString("SexRouletteChallengeDone_"+iCategory.ToString()+"_"+iStep.ToString(),id.ToString());
        }
    }
        /*int iNextId = iGetId();
        listIdQDone.Add(iNextId);
        if(iNextId==-1){
            listIdQDone = new List<int>();
            iNextId = iGetId();
        }
        LocalizedString _MyLocalizedString;
        
        _MyLocalizedString = strSoft+"Sex Game/chalenge/"+iCategory.ToString()+"/"+iStep.ToString()+"/"+iNextId.ToString();
        string str = _MyLocalizedString;
        _MyLocalizedString = strSoft+"Sex Game/time/"+iCategory.ToString()+"/"+iStep.ToString()+"/"+iNextId.ToString();
        iTimerQuestion = int.Parse(_MyLocalizedString.ToString());
        
        tmpPlayerName.text = p1.name;
        str = str.Replace("{Player1_Allsex}",p1.name);
        str = str.Replace("{Player1_Men}",p1.name);
        str = str.Replace("{Player1_Women}",p1.name);
        str = str.Replace("{Player2_Allsex}",p2.name);
        str = str.Replace("{Player2_Men}",p2.name);
        str = str.Replace("{Player2_Women}",p2.name);
        foreach(string cat in data.Categories){
            if(str.Contains("{"+cat+"}")){
                Debug.Log("Chalenge contain " + cat);
                List<string> list = new List<string>();
                foreach(ObjectSexGame obj in data.Objects ){
                    if(obj.selected && obj.category.Contains(cat)) list.Add(obj.name);
                }
                str = str.Replace("{"+cat+"}",list[UnityEngine.Random.Range(0,list.Count)]);
            }
        }
        tmpContent.text = str;
        
    }
    int iGetId () {
        /*LocalizedString _MyLocalizedString = strSoft+"Sex Game/number/"+iCategory.ToString()+"/"+iStep.ToString();
        int iMax = int.Parse(_MyLocalizedString.ToString());
        Debug.Log(iMax + " -> " + strSoft+"Sex Game/number/"+iCategory.ToString()+"/"+iStep.ToString());
        int iQId = -1;
        int iTry = 0 ;
        while(iQId<0 && iTry <50){
            int iRandomId = UnityEngine.Random.Range(1,iMax+1);
            if(bQuestionOk(iRandomId)){
                iQId = iRandomId;
            }
            iTry++;
        }
        Debug.Log("Final ID " + iQId);
        _MyLocalizedString = strSoft+"Sex Game/chalenge/"+iCategory.ToString()+"/"+iStep.ToString()+"/"+iQId.ToString();
        Debug.Log(_MyLocalizedString);
        //return iQId;
    }
    bool bQuestionOk ( int id ) {
        bool res = false;
        if(listIdQDone.Contains(id)) return false;
        
        LocalizedString _MyLocalizedStringSex = strSoft+"Sex Game/targetp1/"+iCategory.ToString()+"/"+iStep.ToString()+"/"+id.ToString();
        if(p1.isMale && _MyLocalizedStringSex=="{Player1_Women}") return false;
        if(!p1.isMale && _MyLocalizedStringSex=="{Player1_Men}") return false;
        
        LocalizedString _MyLocalizedString = strSoft+"Sex Game/chalenge/"+iCategory.ToString()+"/"+iStep.ToString()+"/"+id.ToString();
        string strChalenge = _MyLocalizedString;
        if( strChalenge.Contains("Player2_Women") && p2.isMale ) return false;
        if( strChalenge.Contains("Player2_Men") && !p2.isMale ) return false;
            
        foreach(string cat in data.Categories){
            if(strChalenge.Contains("{"+cat+"}")){
                bool bSelected = false;
                foreach(ObjectSexGame obj in data.Objects){
                    if(obj.selected && obj.category.Contains(cat)) bSelected = true;
                }
                if(!bSelected) return false;
            }
        }
                
        return true;
        }*/
    void endspin () {
        bWaitForEndSpin = false;
        LikeDislike.SetActive(true);
        if(currentChallenge.timer==0){
            tmpSpin.text = "OK";
            NextQuestionButton.SetActive(true);
        }
    }
    
    public void gonextQuestion () {
        Debug.Log("CLICK NEXT");
        if(!anim.GetCurrentAnimatorStateInfo(0).IsName("result")) return;
        if(iStep>=5 && !data.bScenarioCatAvailable(iCategory))
        {
            Premium.ShowPremium("Sex_" + iCategory.ToString() + "_GameOver", data.GameType.Scenario, 2);
            return;
        }
        Debug.Log("GO NEXT");
        NextQuestionButton.SetActive(false);
        LikeDislike.SetActive(false);
        iTotQuestion++;
        EventManager.Instance.SendEvent("Sex_In_Progress", iTotQuestion.ToString(), true, true, false, false);
        iQuestionInStep++;
        if(iQuestionInStep>=iListQuestionNumber[iStep]){
            iStep++;
            iQuestionInStep=0;
            if(iStep>=iListQuestionNumber.Length){
                GameOverPanel.SetActive(true);
                EventManager.Instance.IncreaseTrigger("GameOver", true, true, true, false);
                EventManager.Instance.IncreaseTrigger("GameOverSex", true, true, true, false);
                //OneSignalInit.IncreaseAmountForOneSignalTrigger("GameOver");
                //OneSignalInit.IncreaseAmountForOneSignalTrigger("GameOverSex");
                return;
            }
        }
        Debug.Log("Step : " + iStep.ToString() + " - " + iQuestionInStep + "/" +iListQuestionNumber[iStep] );
        imgThermo.fillAmount = ((float)iTotQuestion)/25f;//(float)(iStep+1)/9f;

        rectThermoIcon.anchorMin = new Vector2(0.5f, imgThermo.fillAmount);
        rectThermoIcon.anchorMax = new Vector2(0.5f, imgThermo.fillAmount);
        rectThermoIcon.anchoredPosition = new Vector2(0, 0);
        Player temp = p1;
        p1 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        while (p1 == temp) p1 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];
        while (p1 == p2) p2 = data.players[UnityEngine.Random.Range(0, data.players.Count)];

        tmpPlayerName.text = p1.name;
        timer.bStart=false;
        string _MyLocalizedString = I2LocManager.get_text("Spin");
        tmpSpin.text = _MyLocalizedString;
        anim.SetTrigger("endresult");
        if(iTotQuestion>6 && !PlayerPrefs.HasKey("sexrouletterated_sexgame")){
            PlayerPrefs.SetInt("sexrouletterated_sexgame",1);
            data.ShowRateUsPopup();
        }
    }
    public void Like ( bool like ) {
        string query = "UPDATE SexRouletteGame SET good = good+1 WHERE id = " + currentChallenge.id.ToString(); 
        if(!like) query = "UPDATE SexRouletteGame SET bad = bad+1 WHERE id = " + currentChallenge.id.ToString(); 
        string url = "https://phpstack-156860-474885.cloudwaysapps.com/ToD/postupdate.php";
        WWWForm form = new WWWForm();
        form.AddField("query",query);
        UnityWebRequest uwr = UnityWebRequest.Post(url,form);
        uwr.SendWebRequest();
        LikeDislike.SetActive(false);
        if(currentChallenge.timer==0) gonextQuestion();
        if (like) EventManager.Instance.IncreaseTrigger("SexLiked", true, true, true, false);
        else EventManager.Instance.IncreaseTrigger("SexDisliked", true, true, true, false);
    }
    void FailLoad () {
        StanManager.ShowMessage1CTA("Error", "Connexion Error, please connect to internet and try again", "OK", () => {
            manager.clickBack();
        });
    }
    public void ClickThermo ()
    {
        Tips.SetActive(true);
    }
    public void HideThermoTips ( bool bOk)
    {
        Tips.SetActive(false);
        if (bOk)
        {
            Premium.ShowPremium("Sex_Tips_" + iCategory.ToString(), data.GameType.Scenario,1);
        }
    }
    public void GoHot ()
    {
        Premium.ShowPremium("Sex_GoHot_" + iCategory.ToString(), data.GameType.Scenario,1);
    }
}
