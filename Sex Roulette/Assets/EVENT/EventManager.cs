﻿//#define No_One_Signal
#define No_AppsFlyer

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class EventManager : MonoBehaviour
{
    public string BASE_URL;
    public static EventManager Instance;
    public List<EventResponse> list_popup;

    public void DEBUGTESTEVENT ( string event_name)
    {
        SendEvent(event_name, "1", true, false, false, false);
    }

    [Serializable]
    public class ResultAction
    {
        public string id, action;
    }
    private void Awake()
    {
        if (Instance) Destroy(gameObject);
        else Instance = this;
    }
    private void Start()
    {
        UIGTM.Instance.EventResultEvent += EventTriggerHandler;
    }


    public void SendEvent(string trigger_name , string trigger_value, bool bServer, bool bOneSignalTrigger, bool bOneSignalTag, bool bAppsFlyer)
    {
        if (bServer)
        {
            StartCoroutine(_SendTrigger(trigger_name, trigger_value));
        }
        if (bOneSignalTrigger)
        {
#if !No_One_Signal
            OneSignal.AddTrigger(trigger_name, trigger_value);
#endif
        }
        if (bOneSignalTag)
        {
#if !No_One_Signal
            OneSignal.SendTag(trigger_name, trigger_value);
#endif
        }
        if (bAppsFlyer)
        {
#if !No_AppsFlyer
            Dictionary<string, string> eventValues = new Dictionary<string, string>();
            eventValues.Add(trigger_name, trigger_value);
            AppsFlyer.sendEvent(name, eventValues);
#endif
        }
    }
    public void RemoveEvent ( string trigger_name, bool bServer, bool bOneSignalTag)
    {
        PlayerPrefs.DeleteKey(trigger_name);
        if (bServer)
        {
            WWWForm form = new WWWForm();
            form.AddField("userid", AccountManager.Instance.account.pk);
            form.AddField("event", trigger_name);
            UnityWebRequest uwr = UnityWebRequest.Post(BASE_URL + "/user/event/remove", form);
            uwr.SendWebRequest();
        }
        if (bOneSignalTag)
        {
#if !No_One_Signal
            OneSignal.DeleteTag(trigger_name);
#endif
        }
    }
    [Serializable]
    public class EventResponse
    {
        public List<EventResponseDetail> response;
        //public string response;
    }
    [Serializable]
    public class EventResponseDetail
    {
        public int id, display_id;
        public string name, url, tracking;
    }

    IEnumerator _SendTrigger (string trigger_name, string trigger_value)
    {
        DebugEditor("Send new trigger");
        WWWForm form = new WWWForm();
        form.AddField("userid", AccountManager.Instance.account.pk);
        form.AddField("usersignature", AccountManager.Instance.account.signature);
        form.AddField("version", get_version());
        form.AddField("event", trigger_name);
        form.AddField("value", trigger_value);
        UnityWebRequest uwr = UnityWebRequest.Post(BASE_URL+"/user/event/send", form);
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            DebugEditor(uwr.error);
        }
        else
        {
            DebugEditor(uwr.downloadHandler.text);
            try
            {
                list_popup = JsonHelper.getJsonArray<EventResponse>("[" + uwr.downloadHandler.text + "]");
                if (list_popup.Count > 0)
                {
                    foreach (EventResponseDetail popup in list_popup[0].response)
                    {
                        UIGTM.Instance.ShowPopupFromUrl(popup.url, popup.name, popup.display_id);
                    }
                }
            }
            catch (Exception e)
            {
                //fatal_error = true;
                Debug.LogException(e, this);
            }
        }
    }
    string get_version ()
    {
        return AccountManager.Instance.account.device + "_" + AccountManager.Instance.account.version;
    }
    public void IncreaseTrigger ( string trigger_name, bool bServer, bool bOneSignalTrigger, bool bOneSignalTag, bool bAppsFlyer)
    {
        if (PlayerPrefs.HasKey(trigger_name))
        {
            PlayerPrefs.SetInt(trigger_name, PlayerPrefs.GetInt(trigger_name) + 1);
        }
        else
        {
            PlayerPrefs.SetInt(trigger_name, 1);
        }
        int value = PlayerPrefs.GetInt(trigger_name);
        SendEvent(trigger_name, value.ToString(), bServer, bOneSignalTrigger, bOneSignalTag, bAppsFlyer);
    }
    public void SendAppsFlyerPurchase(string currency, string price)
    {
#if !No_AppsFlyer
        Dictionary<string, string> eventValues = new Dictionary<string, string>();
        eventValues.Add(AFInAppEvents.CURRENCY, currency);
        eventValues.Add(AFInAppEvents.REVENUE, price);
        eventValues.Add("af_quantity", "1");
        AppsFlyer.sendEvent(AFInAppEvents.PURCHASE, eventValues);
#endif
    }

    void DebugEditor ( string str)
    {
#if UNITY_EDITOR
        Debug.Log("Safe:" + str);
#endif
    }


    ///CATCH EVENT
    void EventTriggerHandler(UIGTM_Class.ActionDetail action)
    {
        DebugEditor("Event Handler " + action.name);
        if (!string.IsNullOrEmpty(action.trigger_name))
        {
            DebugEditor("Have trigger");
            SendEvent(action.trigger_name, action.trigger_value, action.bToServer, action.bToOneSignalTrigger, action.bToOneSignalTag, action.bToAppsFlyer);
        }
        switch (action.type)
        {
            case UIGTM_Class.ActionType.ContactUs:
                Special.ContactUs();
                break;
            case UIGTM_Class.ActionType.OpenUrl:
                Application.OpenURL(action.detail);
                break;
            case UIGTM_Class.ActionType.OpenUrlLocal:
                Special.OpenUrlLocally(action.detail,"");
                break;
            case UIGTM_Class.ActionType.RateUs:
                Special.RateUs();
                break;
            case UIGTM_Class.ActionType.RequestNotif:
                OneSignalManager.Instance.TriggerAskNotif();
                break;
            case UIGTM_Class.ActionType.Purchase:
                IAPManager.PurchasePack(action.detail, action.name);
                break;
        }
    }
#if UNITY_ANDROID
    void OnApplicationPause(bool isFocus)
    {
        //True quand quitte
        //false quand reviens
        isFocus = !isFocus;
#else
    void OnApplicationFocus(bool isFocus){
        //false quand quitte
        //true quand reviens
#endif
        Debug.Log("App focus : " + isFocus);
        if (!isFocus)
        {
            IncreaseTrigger("App_Closed", true, false, false, false);
        }
    }
}
