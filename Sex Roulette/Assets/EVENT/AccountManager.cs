﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class AccountManager : MonoBehaviour
{

    public static AccountManager Instance;
    public Account account =new Account();
    public bool is_init = false;
    [System.Serializable]
    public class Account
    {
        public int pk;
        public string signature = "";
        public string device = "";
        public string version = "";
        public string country = "";
        public string city = "";
        public string country_code = "";
        public string language = "";
        public string application_name = "";
        public string name = "";
        public string email = "";
        public string onesignal = "";
        //public string events = "";
    }
    private string folderPath;
    private string filePath;

    void Awake()
    {
        if (Instance != null) { Destroy(gameObject); }
        else
        {
            Instance = this;
            folderPath = Application.persistentDataPath + "/Account";
            filePath = folderPath + "/account.json";
        }
    }
    private void Start()
    {
        if (!System.IO.Directory.Exists(folderPath))
        {
            System.IO.Directory.CreateDirectory(folderPath);
        }
        if(System.IO.File.Exists(filePath))
        {
            account = JsonUtility.FromJson<Account>(System.IO.File.ReadAllText(filePath));
            if (account.version != data.AppVersion) SetVersion(data.AppVersion);
            is_init = true;
        }
        else
        {
            account = new Account();
            account.application_name = data.AppName;
            account.language = get_language();
            account.version = data.AppVersion;
            account.signature = data.WriterId;
#if UNITY_IPHONE
            account.device = "iOS";
#elif UNITY_ANDROID
            account.device = "Android";
#else
            account.device = "Unity";
#endif
            CreateAccount();
        }
    }

    public void CreateAccount()
    {
        StartCoroutine(_CreateAccount());
    }
    [System.Serializable]
    public class ResponseAccount
    {
        public int user_id;
    }
    IEnumerator _CreateAccount() { 
        WWWForm form = new WWWForm();
        form.AddField("usersignature", account.signature);
        form.AddField("language", account.language);
        form.AddField("application", account.application_name);
        form.AddField("device", account.device);
        form.AddField("version", account.version);
        UnityWebRequest uwr = UnityWebRequest.Post(EventManager.Instance.BASE_URL + "/user/account/create", form);
        uwr.timeout = 7;
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            DebugEditor(uwr.error);
        }
        else
        {
            DebugEditor(uwr.downloadHandler.text);
            ResponseAccount response = JsonUtility.FromJson<ResponseAccount>(uwr.downloadHandler.text);
            account.pk = response.user_id;
            SaveInfo();
        }
        is_init = true;
    }
    public void SetName(string name)
    {
        if (name == account.name) return;
        account.name = name;
        RefreshInfo();
    }
    public void SetEmail(string email)
    {
        if (email == account.email) return;
        account.email = email;
        RefreshInfo();
    }
    public void SetOneSignal(string os)
    {
        if (os == account.onesignal) return;
        account.onesignal = os;
        RefreshInfo();
    }
    public void SetUserInfo(string country, string city, string countrycode)
    {
        if (country == account.country && city == account.city && countrycode == account.country_code) return;
        account.country = country;
        account.city = city;
        account.country_code = countrycode;
        RefreshInfo();
    }
    public void SetVersion(string version)
    {
        if (version == account.version) return;
        account.version = version;
        RefreshInfo();
    }
    public void RefreshInfo()
    {
        Debug.Log("Refreshing");
        WWWForm form = new WWWForm();
        if (!string.IsNullOrEmpty(account.country)) form.AddField("country", account.country);
        if (!string.IsNullOrEmpty(account.city)) form.AddField("city", account.city);
        if (!string.IsNullOrEmpty(account.country_code)) form.AddField("country_code", account.country_code);
        if (!string.IsNullOrEmpty(account.name)) form.AddField("name", account.name);
        if (!string.IsNullOrEmpty(account.email)) form.AddField("email", account.email);
        if (!string.IsNullOrEmpty(account.onesignal)) form.AddField("one_signal", account.onesignal);
        //if (!string.IsNullOrEmpty(account.events)) form.AddField("events", account.events);
        if (!string.IsNullOrEmpty(account.device)) form.AddField("device", account.device);
        if (!string.IsNullOrEmpty(account.version)) form.AddField("version", account.version);
        form.AddField("userid", account.pk);
        form.AddField("usersignature", account.signature);
        UnityWebRequest uwr = UnityWebRequest.Post(EventManager.Instance.BASE_URL+ "/user/account/edit", form);
        uwr.SendWebRequest();
        SaveInfo();
    }
    void SaveInfo ()
    {
        System.IO.File.WriteAllText(filePath, JsonUtility.ToJson(account));
    }

    void DebugEditor(string str)
    {
#if UNITY_EDITOR
        Debug.Log("Safe:" + str);
#endif
    }

    string get_language()
    {
        if (Application.systemLanguage == SystemLanguage.French) return "French";
        else if (Application.systemLanguage == SystemLanguage.English) return "English";
        else if (Application.systemLanguage == SystemLanguage.German) return "German";
        else if (Application.systemLanguage == SystemLanguage.Spanish) return "Spanish";
        else if (Application.systemLanguage == SystemLanguage.Italian) return "Italian";
        else if (Application.systemLanguage == SystemLanguage.Portuguese) return "Portuguese";
        else if (Application.systemLanguage == SystemLanguage.Czech) return "Czech";
        else if (Application.systemLanguage == SystemLanguage.Danish) return "Danish";
        else if (Application.systemLanguage == SystemLanguage.Swedish) return "Swedish";
        else if (Application.systemLanguage == SystemLanguage.Finnish) return "Finnish";
        else if (Application.systemLanguage == SystemLanguage.Norwegian) return "Norwegian";
        else if (Application.systemLanguage == SystemLanguage.Dutch) return "Dutch";
        else if (Application.systemLanguage == SystemLanguage.Polish) return "Polish";
        else if (Application.systemLanguage == SystemLanguage.Russian) return "Russian";
        else if (Application.systemLanguage == SystemLanguage.Turkish) return "Turkish";
        return "English";
    }
}
