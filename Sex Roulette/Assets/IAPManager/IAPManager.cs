﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class IAPManager : MonoBehaviour, IStoreListener
{
    private string PACK_CLASSIC = "sexrouletteweeklyclassic";
    private static IAPManager instance;
    public static Action<bool> PremiumUnlockedEvent;
   
    private string strBuyFrom = "";
	
    public List<IAPProduct> IAPProducts = new List<IAPProduct>() { new IAPProduct() { productID = "PRODUCT_ID_HERE", productType = ProductType.Subscription } };

    private IStoreController controller;
    private IExtensionProvider extensions;
	private IAppleExtensions m_AppleExtensions;

	private bool m_availableToMakePurchases = false;
	private bool initializedOver = false;
	private float fStart ;

    [System.Serializable]
    public class IAPProduct
    {
	    public string productID;
	    public ProductType productType;
    } 

	//This is the initializer
	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
		if (FindObjectsOfType(GetType()).Length > 1){
			Destroy(gameObject);
		}else{
			instance = this;
            PACK_CLASSIC = data.SplitGroup.inapp;
			ShowDebug("Find " + IAPProducts.Count.ToString());
			ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
	
			foreach (IAPProduct product in IAPProducts)
			{
#if UNITY_ANDROID
                if (product.productID == "Sexroulettepro") product.productID = "sexroulettepro";
                if (product.productID == "Sexrouletteprodiscount") product.productID = "sexrouletteprodiscount";
#endif
                builder.AddProduct(product.productID, product.productType);
			}
			UnityPurchasing.Initialize(this, builder);
			fStart = Time.time;
			ShowDebug("End Awake");
		}
	}
	private void OnDeferred (Product item)
	{
		Debug.Log ("Purchase deferred: " + item.definition.id);
	}
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        ShowDebug("OnInitialized callback");
        this.controller = controller;
        this.extensions = extensions;
	    ShowDebug("Step 1");
	    m_AppleExtensions = extensions.GetExtension<IAppleExtensions> ();
	    
	    ShowDebug("Step 2");
	    m_availableToMakePurchases = true;
        ShowDebug("Step 3");
        data.PriceWeekly = controller.products.WithID(PACK_CLASSIC).metadata.localizedPriceString;
        ShowDebug("Step 4");
        data.fPriceWeekly = (float)controller.products.WithID(PACK_CLASSIC).metadata.localizedPrice;
        ShowDebug("Step 5");
        data.CurrencySymbol = controller.products.WithID(PACK_CLASSIC).metadata.isoCurrencyCode;
        ShowDebug("Step 6");
        GetPreviousPurchase();
        ShowDebug("Is premium : " + data.Premium.ToString());
        ShowDebug("Price weekly : " + data.PriceWeekly);
        Invoke("SendEventPremiumTag", 1f);
	    initializedOver = true;
    }
    void SendEventPremiumTag ()
    {

        Special.PremiumManagerOneSignal();
    }
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        //Code if can't initialize IAP store
	    ShowDebug("Can't initialize IAP store! \nError:" + error.ToString());
	    //data.SendDebugEvent();
    }

    public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
	    Waiting.HideWaiting();
        //Code for when purchases failed here
        EventManager.Instance.IncreaseTrigger("PurchaseCanceled", true,true,true,false);
	    //OneSignalInit.IncreaseAmountForOneSignalTrigger("PurchaseCanceled");
	    ShowDebug("Failed purchase! \nError:" + p.ToString() + "\nProductID:" + i.definition.id);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        ShowDebug("Process purchase");
        //Code for when purchases complete here
        bool validPurchase = true; // Presume valid for platforms with no R.V
        // Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX

        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
            AppleTangle.Data(), Application.identifier);

        try
        {
            // On Apple stores, receipts contain multiple products.
            var result = validator.Validate(e.purchasedProduct.receipt);
            // For informational purposes, we list the receipt(s)
            ShowDebug("Receipt is valid. Contents:");
            foreach (IPurchaseReceipt productReceipt in result)
            {
                ShowDebug(productReceipt.productID);
                ShowDebug(productReceipt.purchaseDate.ToString("yyyy-MM-dd hh:mm:ss"));
                ShowDebug(productReceipt.transactionID);
                //PackIdBought(productReceipt.productID);
                ShowDebug("------------------------");
            }
        }
        catch (IAPSecurityException)
        {
            ShowDebug("Invalid receipt, not unlocking content");
            validPurchase = false;
        }
#endif
        if (validPurchase)
        {
            ShowDebug("------------------------");
            ShowDebug("PACK PURCHASED");
            ShowDebug("Inapp Id" + e.purchasedProduct.definition.id);
            ShowDebug("Transaction Id" + e.purchasedProduct.transactionID);
	        ShowDebug("Price " + e.purchasedProduct.metadata.localizedPrice.ToString());
            ShowDebug("------------------------");
	        PackIdBought(e.purchasedProduct.definition.id);
            SendTransactionToServer(e.purchasedProduct);
            EventManager.Instance.SendEvent("PurchaseCompleted", e.purchasedProduct.definition.id, true, false, false, false);
        }
#if UNITY_EDITOR
        PackIdBought(e.purchasedProduct.definition.id);
#endif
	    Waiting.HideWaiting();
	    return PurchaseProcessingResult.Complete;
    }
    void PackIdBought ( string str)
    {
        ShowDebug("Unlocking 1" + str);
        Waiting.HideWaiting();
        ShowDebug("Unlocking 2");
        EventManager.Instance.SendEvent("Just_Purchased", str, false, false, true, false);
        ShowDebug("Unlocking 3");
        switch (str)
        {
            case "wheelgohot":
                PlayerPrefs.SetInt("wheelunlocked", 1);

                //OneSignalInit.SendTag("Premium_Wheel", "1");
                break;

            case "neverevergohot":
                PlayerPrefs.SetInt("neverunlocked", 1);
                //OneSignalInit.SendTag("Premium_Never", "1");
                break;

            case "sexydicesgohot":
                PlayerPrefs.SetInt("sexroulette_premium_dice", 1);
                //OneSignalInit.SendTag("Premium_Dice", "1");
                break;

            case "sexroulette1":
                PlayerPrefs.SetInt("sexrouletteunlock0", 1);
                //OneSignalInit.SendTag("Premium_SexGame0", "1");
                break;
            case "sexroulette2":
                PlayerPrefs.SetInt("sexrouletteunlock1", 1);
                //OneSignalInit.SendTag("Premium_SexGame1", "1");
                break;
            case "sexroulette3":
                PlayerPrefs.SetInt("sexrouletteunlock2", 1);
                //OneSignalInit.SendTag("Premium_SexGame2", "1");
                break;
            case "Sexrouletteprodiscount": //ios
            case "sexrouletteprodiscount": //android
            case "sexrouletteprodiscount2": //android
                //OneSignalInit.SendTag("Premium_SexGame_All", "1");
                PlayerPrefs.SetInt("sexrouletteunlock0", 1);
                PlayerPrefs.SetInt("sexrouletteunlock1", 1);
                PlayerPrefs.SetInt("sexrouletteunlock2", 1);
                break;
            case "tod4wtf":
                PlayerPrefs.SetInt("tod4buypack1", 1);
                //OneSignalInit.SendTag("Premium_TOD_WTF", "1");
                break;
            case "tod4dirty":
                PlayerPrefs.SetInt("tod4buypack2", 1);
                //OneSignalInit.SendTag("Premium_TOD_DIRTY", "1");
                break;
            case "tod4couple":
                PlayerPrefs.SetInt("tod4buypack3", 1);
                //OneSignalInit.SendTag("Premium_TOD_COUPLE", "1");
                break;
            case "tod4hardcore":
                PlayerPrefs.SetInt("tod4buypack4", 1);
               //OneSignalInit.SendTag("Premium_TOD_HARD", "1");
                break;
            case "tod4all":
            case "tod4all2":
                PlayerPrefs.SetInt("tod4buypack1", 1);
                PlayerPrefs.SetInt("tod4buypack2", 1);
                PlayerPrefs.SetInt("tod4buypack3", 1);
                PlayerPrefs.SetInt("tod4buypack4", 1);
                //OneSignalInit.SendTag("Premium_TOD_All", "1");
                break;
            case "Sexroulettepro": //ios
            case "sexroulettepro": //adnroid
            case "sexrouletteproafter1couple1tod":
            case "sexrouletteproafterdiceswheel":
            case "sexrouletteproafterallcouplealltod":
            case "sexroulettepropromo50":
            case "sexrouletteweekly1":
            case "sexrouletteweekly2":
            case "sexrouletteweekly1promo25":
            case "sexrouletteweekly1promo50":
            case "sexroulettemonthly1":
            case "sexroulettemonthly1promo25":
            case "sexroulettemonthly1promo50":
            case "sexrouletteyearly1":
            case "sexrouletteyearly1promo25":
            case "sexrouletteyearly1promo50":
                data.Premium = true;
                PremiumUnlockedEvent?.Invoke(true);
                //PlayerPrefs.SetInt("sexroulette_premium_dice", 1);
                //PlayerPrefs.SetInt("wheelunlocked", 1);
                //PlayerPrefs.SetInt("neverunlocked", 1);
                //PlayerPrefs.SetInt("sexrouletteunlock0", 1);
                //PlayerPrefs.SetInt("sexrouletteunlock1", 1);
                //PlayerPrefs.SetInt("sexrouletteunlock2", 1);
                //PlayerPrefs.SetInt("tod4buypack1", 1);
                //PlayerPrefs.SetInt("tod4buypack2", 1);
                //PlayerPrefs.SetInt("tod4buypack3", 1);
                //PlayerPrefs.SetInt("tod4buypack4", 1);
                //OneSignalInit.SendTag("Premium_All", "1");
                break;
        }
        ShowDebug("Case : " + str);
    }


    /// <summary>
    /// This function will check if the player is subscribed to
    /// a product with the same id as subscriptionProductID
    /// </summary>
    /// <returns></returns>
   void GetPreviousPurchase ()
    {
        if (!m_availableToMakePurchases)
        {
            ShowDebug("App is not ready to make purchases yet!");
            return;
        }

        foreach (Product product in controller.products.all)
        {
            if (product.hasReceipt)
            {
                foreach (IAPProduct productList in IAPProducts)
                {
                    if (product.definition.id == productList.productID)
                    {
                        //Code for when purchases complete here
                        bool validPurchase = true; // Presume valid for platforms with no R.V.

                        // Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
                        // Prepare the validator with the secrets we prepared in the Editor
                        // obfuscation window.

                        //If these give you an error, make sure to generate the tangle file first with
                        //Window -> Unity IAP -> Receipt Validation Obfuscator

                        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                            AppleTangle.Data(), Application.identifier);

                        try
                        {
                            // On Apple stores, receipts contain multiple products.
                            var result = validator.Validate(product.receipt);
                            // For informational purposes, we list the receipt(s)
                            ShowDebug("Receipt is valid. Contents:");
                            foreach (IPurchaseReceipt productReceipt in result)
                            {
                                if (productReceipt.productID != null) ShowDebug(productReceipt.productID);
                                if (productReceipt.purchaseDate != null) ShowDebug(productReceipt.purchaseDate.ToString("yyyy-MM-dd hh:mm:ss"));
                                if (productReceipt.transactionID != null) ShowDebug(productReceipt.transactionID);
                            }
                        }
                        catch (IAPSecurityException)
                        {
                            ShowDebug("Invalid receipt, not subscribed!");
                            validPurchase = false;
                            return;
                        }
#endif
                        if (validPurchase)
                        {
                            //ShowDebug("Valid Purchase");
                            //PackIdBought(product.definition.id);
                            if (product.definition.type == ProductType.Subscription)
                            {
                                SubscriptionManager manager = new SubscriptionManager(product, null);
                                ShowDebug("manager");
                                SubscriptionInfo info = manager.getSubscriptionInfo();
                                ShowDebug("info");
                                bool m_isPremiumPlayer = (info.isSubscribed() == Result.True) && !(info.isExpired() == Result.True);
                                if (m_isPremiumPlayer) PackIdBought(product.definition.id);
                            }
                            else
                            {
                                PackIdBought(product.definition.id);
                            }
                        }
                    }
                }
            }
        }
        ShowDebug("End premium");
    }

    /// <summary>
    /// Input a purchase id. Make sure a product with the same id exists in IAPProducts
    /// </summary>
    /// <param name="strInappID"></param>

	public static void PurchasePack(string str, string strFrom2) {
		instance._PurchasePack(str,strFrom2);
	}
	void _PurchasePack(string str, string strFrom2) {
        strBuyFrom = strFrom2;
        if (str == "") str = PACK_CLASSIC;
        ShowDebug("Purchase pack " + str + " " + m_availableToMakePurchases.ToString());
        if (m_availableToMakePurchases)
        {
	        Waiting.ShowWaiting();
            controller.InitiatePurchase(str);
        }
        else
        {
            ShowDebug("Store is not yet ready to make purchases!");
        }
    }

	public static void RestorePurchase () {
		instance._RestorePurchase();
	}
	void _RestorePurchase () {
        ShowDebug("Restore");
        strBuyFrom = "Restore";
#if UNITY_IPHONE
        m_AppleExtensions.RestoreTransactions(OnRestoreTransaction);
#endif
    }

    void OnRestoreTransaction(bool success)
    {
        if (success)
        {
            ShowDebug("Restored transactions!");
        }
        else
        {
            ShowDebug("Failed to restore transactions!");
        }
        Waiting.HideWaiting();
    }

    void SendTransactionToServer(Product product)
	{
		string strPlatform = "";
		string strSoft = "0";
		if(data.Soft) strSoft = "1";
#if UNITY_EDITOR
		strSoft = "1";
		strPlatform = "Editor";
#elif UNITY_ANDROID
		strPlatform = "Android";
#else
		strPlatform = "iOS";
#endif
		WWWForm form = new WWWForm();
		form.AddField("userId",data.WriterId);
		form.AddField("inappId", product.definition.id);
		form.AddField("price", controller.products.WithID(product.definition.id).metadata.localizedPriceString);
		form.AddField("orderId", product.transactionID);
		ShowDebug("Sent step 4");
		form.AddField("appName", data.AppName);
		form.AddField("appVersion", data.AppVersion);
		ShowDebug("Sent step 4");
		form.AddField("platform", strPlatform);
		form.AddField("soft", strSoft);
		ShowDebug("Sent step 5");
		if(data.user_info!=null) form.AddField("country", data.user_info.countryCode);
		else form.AddField("country", "");
		ShowDebug("Sent step 5BIS");
		form.AddField("language", I2.Loc.LocalizationManager.CurrentLanguage);
		ShowDebug("Sent step 6");
		form.AddField("action", strBuyFrom);
		form.AddField("notification", data.NotificationId);
		ShowDebug("Sent step 7");
		form.AddField("sessionList", data.SessionsList);
		form.AddField("sessionTime", Time.time.ToString("F0"));
		ShowDebug("Sent step 8");
		OneSignalManager.Instance.GetTagsOneSignal((str)=>{
			form.AddField("oneSignal", str);
			string url = "https://phpstack-156860-474885.cloudwaysapps.com/2020/Purchase/Purchase.php";
			UnityWebRequest uwr = UnityWebRequest.Post(url,form);
			uwr.SendWebRequest();
			ShowDebug("Sent sent");
		});
		ShowDebug("Sent step 9");
    }
    void ShowDebug(string str)
    {
#if UNITY_EDITOR
        Debug.Log(str);
#endif
        data.Debugs.Add(str);
    }
}